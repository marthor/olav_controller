#pragma once

#include <iostream>
#include <stdexcept>
#include <modbus.h>
#include "olav_controller/vehicle_gears.h"
#include "olav_controller/vehicle_measurements.h"
#include <bitset>


class PlcInterface
{
public:

  PlcInterface(std::string address, int port, VehicleMeasurements* olavMeasurements);

  ~PlcInterface();

  void open();

  void close();

  bool isOpen()
  { return this->connected; }

  void readData();

  void writeData(const double& steeringEffort, const double& breakEffort, const double& throttleEffort,
                 const bool& ignitionOn, const bool& emergencyStop, const  bool& starterEngineOn, const Gears& selectGear);

  void enableSteeringPid(int p, int i, int d);

  void disableSteeringPid();
  
//  void setDataToPLC(int steeringEffort, int breakEffort, int throttleEffort, bool ignitionOn, bool starterEngineOn, Gears selectGear);
  
//  int getCurrentSteeringAngleRaw();
//  Gears getCurrentGear();
//  int getCurrentBrakePos();
//  bool getCurrentIgnitionOn();
//  bool getCurrentAutonomousModeSwitchOn();
    

private:
 
//  struct DataToPlc
//  {
//    int16_t steeringEffort;
//    int16_t breakEffort;
//    int16_t throttleEffort;
//    int16_t ignitionOn;
//    int16_t starterEngineOn;
//    int16_t emergencyStopOn;
//    int16_t selectGear;
//  }dataToPlc;
//
//  struct DataFromPlc
//  {
//    int16_t currentSteeringAngleRaw;
//    int16_t currentGear;
//    int16_t currentBreakPos;
//    int16_t currentIgnitionOn;
//    int16_t currentAutonomousModeSwitchOn;
//  }dataFromPlc;

  VehicleMeasurements* olavMeasurements;
  modbus_t* modbusContext;
  std::string address;
  int port;
  bool connected;
  bool ticker;
};


struct PlcInterfaceException : public std::runtime_error
{
  PlcInterfaceException(const char* message) : std::runtime_error(message)
  {}
};
