#pragma once

#include <ros/ros.h>

#include <diagnostic_updater/diagnostic_updater.h>
#include <diagnostic_updater/publisher.h>

#include "olav_controller/vehicle_states.h"
#include "olav_controller/vehicle_measurements.h"
#include "olav_controller/vehicle_gears.h"
#include "olav_controller/vehicle.h"


class OlavDiagnostics
{
public:

  OlavDiagnostics(diagnostic_updater::Updater& diag_updater);

  ~OlavDiagnostics();

  void updateEmergencyStop(const bool& is_emergency_stop)
  { is_emergency_stop_ = is_emergency_stop; };

  void updateEmergencyStopReasonIsEmergencyStopButtonPressed(const bool& is_emergency_stop_button_pressed)
  { is_emergency_stop_button_pressed_ = is_emergency_stop_button_pressed; };

  void updateIsOkToDriveReasonIsReceivingHeartbeats(const bool& is_receiving_hearbeats, const double& time_since_last_message)
  {
    is_receiving_hearbeats_ = is_receiving_hearbeats;
    time_since_last_heartbeat_ = time_since_last_message;
  }

  void updateEmergencyStopReasonIsArduinoOk(const bool& is_arduino_ok)
  { is_arduino_ok_ = is_arduino_ok; };

  void updateEmergencyStopReasonIsMoodusOk(const bool& is_modbus_ok)
  { is_modbus_ok_ = is_modbus_ok; };

  void updateEmergencyStopReasonIsSpeedOk(const bool& is_speed_ok)
  { is_speed_ok_ = is_speed_ok; };

  void updateEmergencyStopReasonIsEmergencyStopServiceCalled(const bool& is_emergency_stop_service_called)
  { is_emergency_stop_service_called_ = is_emergency_stop_service_called; };

  void updateIsOkToDriveReasonExternalOkToDrive(const bool& is_external_ok_to_drive)
  { is_external_ok_to_drive_ = is_external_ok_to_drive; }

  void updateIsOkToDrive(const bool& is_ok_to_drive)
  { is_ok_to_drive_ = is_ok_to_drive; };

  void updateIsOkToDriveReasonIsDrivableGear(const bool& is_drivable_gear)
  { is_drivable_gear_ = is_drivable_gear; };

  void updateIsOkToDriveReasonIsReceivingVehicleComands(const bool& is_receiving_vehicle_commands, const double& time_since_last_message)
  { is_receiving_vehicle_commands_ = is_receiving_vehicle_commands;
      time_since_last_vehicle_command_ = time_since_last_message;};

  void updateIsOkToDriveReasonIsAutonomousMode(const bool& is_autonomous_mode)
  { is_autonomous_mode_ = is_autonomous_mode; };

  void updateVehicleState(const VehicleStates& state,
                          const VehicleStates& next_state,
                          const VehicleStates& prev_state);

  void updateInternalVehicleStates(const Vehicle::InternalVehicleStates& state,
                                   const Vehicle::InternalVehicleStates& next_state,
                                   const Vehicle::InternalVehicleStates& prev_state);

  void updateVehicleMeasurements(const bool& isIgnitionOn,
                                 const double& rpm,
                                 const Gears& gear,
                                 const double& speedMs);

  void updateIsOkToDriveReasonIsReceivingOdometryMessages(const bool& is_receiving_odometry_messages, const double& time_since_last_message)
  { is_receiving_odometry_messages_ = is_receiving_odometry_messages;
    time_since_last_odometry_message_ = time_since_last_message;};

private:

  void diagnosticsCallbackEmergencyStop(diagnostic_updater::DiagnosticStatusWrapper& status);

  void diagnosticsCallbackOkToDrive(diagnostic_updater::DiagnosticStatusWrapper& status);

  void diagnosticsCallbackVehicleStatus(diagnostic_updater::DiagnosticStatusWrapper& status);

  void diagnosticsCallbackModbus(diagnostic_updater::DiagnosticStatusWrapper& status);

  std::string getVehicleStatesString(VehicleStates state);

  std::string getInternalVehicleStatesString(Vehicle::InternalVehicleStates state);

  std::string getVehicleGearsString(Gears gear);

  bool is_emergency_stop_;
  bool is_emergency_stop_button_pressed_;
  bool is_receiving_hearbeats_;
  bool is_arduino_ok_;
  bool is_modbus_ok_;
  bool is_speed_ok_;
  bool is_emergency_stop_service_called_;

  bool is_ok_to_drive_;
  bool is_drivable_gear_;
  bool is_receiving_vehicle_commands_;
  bool is_autonomous_mode_;
  bool is_external_ok_to_drive_;

  bool is_receiving_odometry_messages_;

  VehicleStates state_;
  VehicleStates next_state_;
  VehicleStates prev_state_;
  Vehicle::InternalVehicleStates internal_state_;
  Vehicle::InternalVehicleStates next_internal_state_;
  Vehicle::InternalVehicleStates prev_internal_state_;
  VehicleMeasurements measurements_;
  bool isIgnitionOn_;
  double rpm_;
  Gears gear_;
  double speedMs_;

  double time_since_last_heartbeat_;
  double time_since_last_vehicle_command_;
  double time_since_last_odometry_message_;
};
