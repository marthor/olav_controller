#pragma once

enum VehicleStates
{
  NO_STATE = 0,
  EMERGENCY_STOP = 1,
  MANUAL_MODE = 2,
  VEHICLE_OFF = 3,
  VEHICLE_ON_PARK = 4,
  VEHICLE_ON_REVERSE = 5,
  VEHICLE_ON_LOW = 6,
  VEHICLE_ON_HIGH = 7,
};
