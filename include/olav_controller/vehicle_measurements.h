#pragma once


#include "olav_controller/vehicle_gears.h"
#include <boost/atomic.hpp>
#include <cmath>


class VehicleMeasurements
{

public:

  VehicleMeasurements(){
    brakePos = 0;
    isIgnitionOn = false;
    isAutonomousModeOn = true;
    isStarterEngineOn = false;
    isGearInPosition = false;
    gear = PARK;
    gearActuatorPosition = 0;
    isEmergencyStop = false;
    rpm = 0;
    roll = 0;
    pitch = 0;
    heading = 0;
    wheelCircumferenceM = 0;
    steeringMeasurement_mVPrRad = 0;
    steeringMeasurement_mVCenter = 0;
    speedMs = 0;
    axelRadPrSec = 0;
    steeringAngleRad = 0;
  }

  int brakePos;
  bool isIgnitionOn;
  bool isAutonomousModeOn;
  bool isStarterEngineOn;
  bool isGearInPosition;
  Gears gear;
  int gearActuatorPosition;
  boost::atomic<bool> isEmergencyStop;
  double rpm;
  double roll;
  double pitch;
  double heading;

  void setWheelCircumferenceM(const double& wheelCircumferenceM) { this->wheelCircumferenceM = wheelCircumferenceM; };
  void setSteeringMeasurement_mVPrRad(const double& steeringMeasurement_mVPrRad) { this->steeringMeasurement_mVPrRad = steeringMeasurement_mVPrRad; };
  void setSteeringMeasurement_mVCenter(const double& steeringMeasurement_mVCenter) { this->steeringMeasurement_mVCenter = steeringMeasurement_mVCenter; };
  void setSteeringAngleRaw(const int& steeringVoltage) { steeringAngleRad = (-1) * (steeringVoltage - steeringMeasurement_mVCenter) / steeringMeasurement_mVPrRad; };
  void setSpeedCentiDegPrSec(const int& axelCentiDegPrSec) { speedMs = ( (0.01 * axelCentiDegPrSec) / 360) * wheelCircumferenceM; };
  double getSpeedMs() { return this->speedMs; };
  double getAxelRadPrSec() { return this->deg2rad(axelRadPrSec); };
  double getSteeringAngleDeg() { return this->rad2deg(steeringAngleRad); };
  double getSteeringAngleRad() { return this->steeringAngleRad; };

private:

  double wheelCircumferenceM;
  double steeringMeasurement_mVPrRad;
  double steeringMeasurement_mVCenter;
  boost::atomic<double> speedMs;
  boost::atomic<double> axelRadPrSec;
  double steeringAngleRad;

  double rad2deg(double rad) { return rad * 180 / M_PI; };
  double deg2rad(double deg) { return deg * M_PI / 180; };
};