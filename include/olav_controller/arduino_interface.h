#pragma once

#include <stdexcept>
#include <stdint.h>
#include <boost/thread.hpp>
#include <boost/atomic.hpp>
#include <serial/serial.h>
#include <chrono>
#include "olav_controller/vehicle_measurements.h"


class ArduinoInterface
{
public:

  ArduinoInterface(std::string serialPortName, uint32_t baudrate, uint32_t timeout, VehicleMeasurements* olavMeasurements);

  ~ArduinoInterface();

  void open();

  void close();

  bool isOpen();

  bool hasNewData();

  bool isReceivingData();

private:

  void worker();

  boost::thread workerThread;
  boost::atomic<bool> runWorker;
  enum WorkerStatus
  {
    stopped, connected, disconnected
  };
  boost::atomic<WorkerStatus> workerStatus;
  boost::atomic<bool> newData;
  serial::Serial serialPort;
  VehicleMeasurements* olavMeasurements;
  std::chrono::system_clock::time_point lastMsg;
};


struct ArduinoInterfaceException : public std::runtime_error
{
  ArduinoInterfaceException(const char* message) : std::runtime_error(message)
  {}
};
