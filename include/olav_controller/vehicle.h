#pragma once

#include <control_toolbox/pid.h>
#include "olav_controller/vehicle_states.h"
#include "olav_controller/vehicle_gears.h"
#include "olav_controller/vehicle_measurements.h"
#include "olav_controller/vehicle_command.h"
#include "olav_controller/olav_feedforward_speed_controller.h"
#include "olav_controller/arduino_interface.h"
#include "olav_controller/plc_interface.h"
#include "olav_controller/moving_average_filter.h"
#include <ackermann_msgs/AckermannDriveStamped.h>
#include <chrono>


class Vehicle
{
public:

  enum InternalVehicleStates
  {
    NO_STATE = 0,
    IGNITION_OFF = 1,
    IGNITION_ON_ENGINE_OFF = 2,
    IGNITION_ON_ENGINE_STARTING = 3,
    ENGINE_ON_PARK = 4,
    ENGINE_ON_NEUTRAL = 5,
    ENGINE_ON_REVERSE = 6,
    ENGINE_ON_LOW = 7,
    ENGINE_ON_HIGH = 8,
    ENGINE_ON_CHANGING_GEAR = 9,
    ENGINE_ON_ENGAGING_GEAR = 10,
  };

  // Data

  VehicleMeasurements olavMeasurements; // Data from Arduino, PLC and odometry message
  VehicleCommand olavCommand; // Data to Olav
  ros::Time timeLastOdometryMsg;
  double odometry_timeout;

  // Constructor, destructor and initializer

  Vehicle(int loopRate);

  ~Vehicle();

  void setupVehicle();

  // Methods

  InternalVehicleStates getCurrentVehicleState();

  InternalVehicleStates getPrevVehicleState();

  InternalVehicleStates getNextVehicleState();

  ackermann_msgs::AckermannDrive getAckermannDriveAverage();

  ackermann_msgs::AckermannDrive getAckermannDriveFullRate();

  bool arduinoIsOK();

  bool modbusIsOK();

  bool isReceivingOdometry();

  bool isEmergencyStop();

  bool isManualMode();

  bool isAutonomousMode();

  void setVehicleState(VehicleStates vehicleState);

  void breaksOnAndReset(int breakEffort = 80);

  void setTargets(double speedMs, double accelMs2, double steeringRad, double steeringVelRadPrS);

  void resetControllers();

  void emergencyStop();

  void updatePLC();

  void updateInternalState(VehicleStates externalState);

  void setAckermannDriveAverageSize(const int& rate);

  void updateMeasurements();

  double getTimeSinceLastOdometryMessage()
  { return time_since_last_odometry_; };

  bool isInGear();

private:

  // Data

  control_toolbox::Pid speedControllerPID;
  control_toolbox::Pid steeringControllerPID;
  VehicleStates vehicleState;
  InternalVehicleStates internalState;
  InternalVehicleStates prevInternalState;
  InternalVehicleStates nextInternalState;
  int loopCounter;
  int loopRate;
  int reconnectCounter;
  bool plcOpenLast;
  PlcInterface* plcInterfacePtr;
  olav_controller::FeedforwardSpeedController* feedforwardSpeedController;
  ArduinoInterface* arduinoInterface;
  bool firstMsg;
  ackermann_msgs::AckermannDriveStamped ackermann_drive_current;
  ackermann_msgs::AckermannDriveStamped ackermann_drive_last;
  ackermann_msgs::AckermannDriveStamped ackermann_drive_average;
  MovingAverageFilter averageSteeringAngle;
  MovingAverageFilter averageSteeringAngleVelocity;
  MovingAverageFilter averageSpeed;
  MovingAverageFilter averageAcceleration;
  MovingAverageFilter averageJerk;
  double time_since_last_odometry_ = NAN;
  double gear_shift_throttle_{0};

  // Methods

  double rampSetPoint(double setPoint, double targetSetPoint, double rate, double dt);

  InternalVehicleStates translateGearExternalStates(VehicleStates externalState);


};
