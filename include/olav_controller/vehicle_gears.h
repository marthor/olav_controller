#pragma once

enum Gears
{
  PARK = 1,
  REVERSE = 2,
  NEUTRAL = 3,
  LOW = 4,
  HIGH = 5,
};
