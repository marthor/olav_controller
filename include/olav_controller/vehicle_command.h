#pragma once

#include <chrono>

class VehicleCommand
{
public:

  VehicleCommand()
  {
    // Dynamic reconfigure parameters

    minCommandedSpeedBeforeBraking = 0;
    maxCommandedSpeed = 0;
    maxCommandedAcceleration = 0;
    maxCommandedThrottleEffort = 0;
    maxCommandedSteeringAngleRad = 0;
    maxCommandedSteeringVelocityRadPrSec = 0;
    maxCommandedSteeringEffort = 0;
    maxRpmInGearChange = 2000;

    // Speed controller variables

    inclination = 0;
    targetSpeed = 0;
    targetAcceleration = 0;
    setpointSpeed = 0;
    feedforwardEffort = 0;
    speedError = 0;
    pidThrottleEffort = 0;
    throttleEffort = 0;
    breakEffort = 0;


    // Steering controller variables

    targetSteeringAngleRad = 0;
    targetSteeringVelocityRadPrSec = 0;
    setpointSteeringAngleRad = 0;
    steeringAngleErrorDeg = 0;
    pidSteeringEffort = 0;
    steeringEffort = 0;


    // Vehicle control variables

    ignitionOn = false;
    starterEngineOn = false;
    gear = PARK;


    // Status variables

    useFeedForward = false;
    timeLast.fromSec(0);
  }


  void clearSpeedControllerVariables()
  {
    inclination = 0;
    targetSpeed = 0;
    targetAcceleration = 0;
    setpointSpeed = 0;
    feedforwardEffort = 0;
    speedError = 0;
    pidThrottleEffort = 0;
    throttleEffort = 0;
    breakEffort = 0;
    timeLast.fromSec(0);
  }


  void clearSteeringControllerVariables()
  {
    targetSteeringAngleRad = 0;
    targetSteeringVelocityRadPrSec = 0;
    setpointSteeringAngleRad = 0;
    steeringAngleErrorDeg = 0;
    pidSteeringEffort = 0;
    steeringEffort = 0;
    timeLast.fromSec(0);
  };


  // Dynamic reconfigure parameters

public:
  double minCommandedSpeedBeforeBraking;
  double maxCommandedSpeed;
  double maxCommandedAcceleration;
  double maxCommandedThrottleEffort;
  double maxCommandedSteeringEffort;
  int maxRpmInGearChange;

  double getMaxCommandedSteeringAngleRad() { return maxCommandedSteeringAngleRad; };
  void setMaxCommandedSteeringAngleRad(const double& angle) { maxCommandedSteeringAngleRad = angle; };
  double getMaxCommandedSteeringAngleDeg() { return rad2deg(maxCommandedSteeringAngleRad); };
  void setMaxCommandedSteeringAngleDeg(const double& angle) { maxCommandedSteeringAngleRad = deg2rad(angle); };
  double getMaxCommandedSteeringVelocityRadPrSec() { return maxCommandedSteeringVelocityRadPrSec; };
  void setMaxCommandedSteeringVelocityRadPrSec(const double& rate) { maxCommandedSteeringVelocityRadPrSec = rate; };
  double getMaxCommandedSteeringVelocityDegPrSec() { return rad2deg(maxCommandedSteeringVelocityRadPrSec); };
  void setMaxCommandedSteeringVelocityDegPrSec(const double& rate) { maxCommandedSteeringVelocityRadPrSec = deg2rad(maxCommandedSteeringVelocityRadPrSec); }

private:
  double maxCommandedSteeringAngleRad;
  double maxCommandedSteeringVelocityRadPrSec;


  // Speed controller variables

public:
  double targetSpeed;
  double targetAcceleration;
  double setpointSpeed;
  double feedforwardEffort;
  double speedError;
  double pidThrottleEffort;
  double throttleEffort;
  double breakEffort;
  double inclination;


  // Steering controller variables

public:
  double steeringAngleErrorDeg;
  double pidSteeringEffort;
  double steeringEffort;

  double getSetpointSteeringAngleRad() { return setpointSteeringAngleRad; };
  void setSetpointSteeringAngleRad(const double& angle) { setpointSteeringAngleRad = angle; };
  double getSetpointSteeringAngleDeg() { return rad2deg(setpointSteeringAngleRad); };
  void setSetpointSteeringAngleDeg(const double& angle) { setpointSteeringAngleRad = deg2rad(angle); };
  double getTargetSteeringAngleRad() { return targetSteeringAngleRad; };
  void setTargetSteeringAngleRad(const double& angle) { targetSteeringAngleRad = angle; };
  double getTargetSteeringAngleDeg() { return rad2deg(targetSteeringAngleRad); };
  void setTargetSteeringAngleDeg(const double& angle) { targetSteeringAngleRad = deg2rad(angle); };
  double getTargetSteeringVelocityRadPrSec() { return targetSteeringVelocityRadPrSec; };
  void setTargetSteeringVelocityRadPrSec(const double& rate) { targetSteeringVelocityRadPrSec = rate; };
  double getTargetSteeringVelocityDegPrSec() { return rad2deg(targetSteeringVelocityRadPrSec); };
  void setTargetSteeringVelocityDegPrSec(const double& rate) { targetSteeringVelocityRadPrSec = deg2rad(rate); };

private:
  double targetSteeringAngleRad;
  double targetSteeringVelocityRadPrSec;
  double setpointSteeringAngleRad;


  // Vehicle control variables

public:
  bool ignitionOn;
  bool starterEngineOn;
  Gears gear;

  // Status variables

public:
  bool useFeedForward;
  ros::Duration dt;
  ros::Time timeLast;

private:

  double rad2deg(double rad) { return rad * 180 / M_PI; };
  double deg2rad(double deg) { return deg * M_PI / 180; };

};