//
// Created by mbk on 08.12.16.
//

#ifndef PROJECT_CLASS_MOVING_AVERAGE_FILTER_H
#define PROJECT_CLASS_MOVING_AVERAGE_FILTER_H

#include <iostream>
#include <boost/circular_buffer.hpp>


class MovingAverageFilter
{
public:

  MovingAverageFilter();

  MovingAverageFilter(const size_t& filter_size);

  void setFilterSize(const size_t& filter_size);

  void clearFilter();

  double getMovingAverage(const double& value);

  void print();

private:

  size_t filter_size_;
  boost::circular_buffer<double> buffer_;
  double calculateMovingAverage();

};


#endif //PROJECT_CLASS_MA_FILTER_H
