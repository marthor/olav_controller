#pragma once

#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>
#include <boost/shared_ptr.hpp>
#include "olav_controller/FeedforwardSpeedControllerConfig.h"


namespace olav_controller
{

class FeedforwardSpeedController
{
public:

  FeedforwardSpeedController(ros::NodeHandle& nodeHandle);

  double computeCommand(double targetSpeedMS, int gear, double inclination);

  bool getEnableFeedforward(){return config.enable_feedforward;};

private:

  void initDynamicReconfig();

  void dynamicReconfigCallback(FeedforwardSpeedControllerConfig config);

  void updateConfig();

  const ros::NodeHandle node;
  boost::shared_ptr<dynamic_reconfigure::Server<FeedforwardSpeedControllerConfig> > reconfigServer;
  FeedforwardSpeedControllerConfig config;
  double P1, P2, P3;
};

}