#!/usr/bin/env python

import sys
import numpy
import pandas
import rosbag_pandas
from scipy import optimize
from matplotlib import pyplot

def determinstic_kinematic_steering_model(parameters,inputs):
    steering_measurment_center = parameters[0]
    steering_measurement_per_rad = parameters[1]
    wheelbase = 2.057
    speed = inputs[0,:]
    steering_measurment = inputs[1,:]
    steering_measurment_centered = steering_measurment - steering_measurment_center
    steering_angle = - steering_measurment_centered/steering_measurement_per_rad
    yaw_rate = -speed/wheelbase*numpy.tan(steering_angle)
    return yaw_rate 

def steering_objective_function(parameters,inputs,measurments):
    yaw_rate_estimate = determinstic_kinematic_steering_model(parameters,inputs)
    error = yaw_rate_estimate - measurments[0,:]
    error = error[~numpy.isnan(error)]
    mse = numpy.mean(numpy.square(error))
    return mse

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Parameter estimation script needs a rosbag as its first argument")
        quit()

    bagname = sys.argv[1];
    print("Importing: " + bagname)
    df = rosbag_pandas.bag_to_dataframe(bagname)
    df = df.interpolate(method='time') # 
    df = df.resample('100L')

    parameters_0 = numpy.array([10250.0, 17000.0]);
    inputs = df[['ugv_sensors_nav_ros_navdata_debug_msg__x_velocity',
                 'olav_vehicle_status__current_steering_angle_raw']].T.values
    measurments = df[['ugv_sensors_nav_ros_navdata_debug_msg__heading_velocity']].T.values
    time = numpy.transpose(df.index.values)

    print('Optimizing...')
    opt_res = optimize.minimize(steering_objective_function,parameters_0,args=(inputs,measurments),method='Nelder-Mead')

    print("Result:")
    print("MSE = " + repr(opt_res.fun))
    print("steering_measurment_mv_center = " + repr(opt_res.x[0]))
    print("steering_measurment_mv_per_rad = " + repr(opt_res.x[1]))

    yaw_rate_estimate = determinstic_kinematic_steering_model(opt_res.x,inputs)
    result = numpy.transpose(numpy.vstack((measurments,yaw_rate_estimate)))
    result_df = pandas.DataFrame(result,index=df.index,columns=['yaw_rate','yaw_rate_est']) 
    result_df.plot()
    pyplot.show()
