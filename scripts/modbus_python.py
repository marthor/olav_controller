#!/usr/bin/python
import numpy
import pygame
import pylibmodbus
import sys
import time
from pygame.locals import *


def get_buttons(joystick):
    for i in range(joystick.get_numbuttons()):
        print(f"{i}: {joystick.get_button(i)}")
    for j in range(joystick.get_numaxes()):
        print(f"{j}: {joystick.get_axis(j)}")
    print("heis")


def main():
    pygame.init()
    pygame.joystick.init()
    joystick = pygame.joystick.Joystick(0)
    joystick.init()
    print(joystick)
    interval = 0.15
    get_buttons(joystick)

    use_modbus = True

    modbus = pylibmodbus.ModbusTcp("172.16.1.3", 502)
    if use_modbus:
        modbus.connect()

    ignition_enable = False
    loop_quit = False
    gear_select = 3
    ticker = True

    # BUTTON_SELECT = 0
    # BUTTON_START = 3
    # BUTTON_L2 = 8
    # BUTTON_L1 = 10
    # BUTTON_TRIANGLE = 12
    # BUTTON_CIRCLE = 13
    # BUTTON_X = 14
    # BUTTON_SQUARE = 15
    BUTTON_A = 0
    BUTTON_B = 1
    BUTTON_X = 2
    BUTTON_Y = 3
    BUTTON_LB = 4
    BUTTON_RB = 5
    BUTTON_SELECT = 6
    BUTTON_START = 7
    BUTTON_MODE = 8
    AXIS_JOY_L_LR = 0
    AXIS_TRIGGER_R = 5

    while not loop_quit:
        max_steering_enabled = joystick.get_button(BUTTON_LB)
        if max_steering_enabled:
            desired_steering_angle = int(joystick.get_axis(AXIS_JOY_L_LR) * 3000.0)
        else:
            desired_steering_angle = int(joystick.get_axis(AXIS_JOY_L_LR) * 1500.0)

        desired_brake_amount = 0
        if joystick.get_button(BUTTON_RB):
            desired_brake_amount = 80
        desired_throttle_amount = int(((joystick.get_axis(AXIS_TRIGGER_R) + 1.0) / 2.0) * 80)
        emergency_stop = joystick.get_button(BUTTON_SELECT)
        ignition_disable = emergency_stop
        engine_start = joystick.get_button(BUTTON_START)
        ignition_enable = joystick.get_button(BUTTON_MODE)
        if joystick.get_button(BUTTON_B):
            gear_select = 1  # P
        if joystick.get_button(BUTTON_Y):
            gear_select = 2  # R
        if joystick.get_button(BUTTON_X):
            gear_select = 3  # N
        if joystick.get_button(BUTTON_A):
            gear_select = 5

        if use_modbus:
            modbus.write_register(0x200 + 0x100, desired_steering_angle)
            modbus.write_register(0x200 + 0x101, desired_brake_amount)
            modbus.write_register(0x200 + 0x102, desired_throttle_amount)
            modbus.write_register(0x200 + 0x103, ignition_enable)
            modbus.write_register(0x200 + 0x104, ignition_disable)
            modbus.write_register(0x200 + 0x105, emergency_stop)
            modbus.write_register(0x200 + 0x106, engine_start)
            modbus.write_register(0x200 + 0x107, gear_select)

            # Alive ticker
            ticker = not ticker
            modbus.write_register(0x200 + 0x10C, ticker)
            # Feedback
            feedback = modbus.read_registers(0x100, 6)
            print("Steering position: ", numpy.int16(feedback[0]))
            ignition_enabled = feedback[5]

        for event in pygame.event.get():
            if event.type == QUIT:
                loop_quit = True
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    loop_quit = True
        time.sleep(interval)
    pygame.quit()
    sys.exit()


if __name__ == '__main__':
    main()
