#!/usr/bin/env python
import os, sys, select, termios, tty
import rospy
from olav_msgs.msg import vehicle_command

modeBindings = {'o':(3),
                'p':(4),
                'r':(5),
                'l':(6),
                'h':(7)}

speedBindings = {'0':(0.0),
                 '1':(1.0),
                 '2':(2.0),
                 '3':(3.0),
                 '4':(4.0),
                 '5':(5.0),
                 '6':(6.0),
                 '7':(7.0),
                 '8':(8.0),
                 '9':(9.0)}

steeringBindings = {'w':0,
                    'a':1,
                    'd':-1,
                    'q':float('inf'),
                    'e':-float('inf')}

msg = """OLAV vehicle control keyboard

Usage:
    While this terminal is active the keyboard can be used to send
    /olav/vehicle_control messages on the topic /olav/vehicle_keyboard_command.
    The controls are listed in the controls section.

Controls:
    Modes: 'O'=Off, 'P'=Park', 'R'=Reverse, 'L'=Low, 'H'=High'
    Speed: '0-9' set speed in meter per second
    Steering: 'Q'=0.5, 'a'+=0.05, 'W'=0.0, 'D'-=0.05, 'E'=-0.5
"""

def getKey():
    global settings
    tty.setraw(sys.stdin.fileno())
    select.select([sys.stdin], [], [], 0)
    key = sys.stdin.read(1)
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
    return key

if __name__ == "__main__":
    os.system('clear')
    print(msg)

    global settings
    settings = termios.tcgetattr(sys.stdin)
    
    global vehicle_control_pub
    vehicle_command_pub = rospy.Publisher('/olav/vehicle_keyboard_command', vehicle_command, queue_size = 10)
    rospy.init_node('vehicle_keyboard_commander')
    rate = rospy.Rate(0.10)
    
    mode = int(0)
    emergency_stop = False
    steering = 0.0
    speed = 0.0
    steering_step = 0.05

    while not rospy.is_shutdown():

        key = getKey()

        if key in modeBindings:
            mode = modeBindings[key]
            speed = 0.0
        elif key in speedBindings:
            speed = speedBindings[key]
        elif key in steeringBindings:
            if (steeringBindings[key]) == 0.0:
                steering = 0.0
            else:
                steering = min(max( steering + steering_step*(steeringBindings[key]), -0.5), 0.5)
        elif key == '\x03':
            break

        vc_msg = vehicle_command()
        vc_msg.vehicle_state.vehicle_state = mode
        vc_msg.ackermann_drive.steering_angle = steering
        vc_msg.ackermann_drive.steering_angle_velocity = float(0.0)
        vc_msg.ackermann_drive.speed = speed
        vc_msg.ackermann_drive.acceleration = float(0.0)
        vc_msg.ackermann_drive.jerk = float(0.0)
        vehicle_command_pub.publish(vc_msg)

        os.system('clear')
        print(msg)
        print("Current command:")
        print(  "    Mode:" + repr(mode) +
              "\n    Speed:" + repr(speed) +
              "\n    Steering:" + repr(steering))

        #rate.sleep()


    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
