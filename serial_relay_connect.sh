#!/usr/bin/env sh
# Sets up a relay  connected to a remote serial port ursng socat.
# SIMULATOR_IP and SIMULATOR_ARDUINO_PORT specifies the IP address and port of the
# computer with the serial port, and SIMULATOR_ARDUINO_SERIAL_PORT specifies the name
# of the created psudo terminal.

if [ -z "$SIMULATOR_IP" ]; then
	echo 'SIMULATOR_IP not set!'
	exit 1
fi

if [ -z "$SIMULATOR_ARDUINO_PORT" ]; then
	echo 'SIMULATOR_ARDUINO_PORT not set!'
	exit 1
fi

if [ -z "$SIMULATOR_ARDUINO_SERIAL_PORT" ]; then
	echo 'SIMULATOR_ARDUINO_SERIAL_PORT not set!'
	exit 1
fi

if [ $ROS_IP = $SIMULATOR_IP ]; then
	echo 'Already on same computer as simulator: No serial port relay needed.'
	exit 0
fi

socat pty,link=$SIMULATOR_ARDUINO_SERIAL_PORT,waitslave tcp-connect:$SIMULATOR_IP:$SIMULATOR_ARDUINO_PORT

exit $?
