#include <gtest/gtest.h>

#include <ctime>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include <olav_simulator/olav_plc_sim.h>
#include "olav_controller/plc_interface.h"
#include "olav_controller/vehicle.h"

class PlcInterfaceTest : public ::testing::Test
{
  protected:
    static OlavPLCSim plc;
    static olav_controller::PlcInterface plcInterface;
    static VehicleData vehicleData;
    static VehicleData vehicleDataReference;

    static void SetUpTestCase()
    {
      vehicleDataReference.setSteeringEffort = -1000;
      vehicleDataReference.setBreakEffort = 10;
      vehicleDataReference.setThrottleEffort = 15;
      vehicleDataReference.setIgnitionOn = true;
      vehicleDataReference.emergencyStop = true;
      vehicleDataReference.setEngineStart = true;
      vehicleDataReference.setSelectedGear = true;
      vehicleDataReference.currentSteeringAngleRaw = 12500;
      vehicleDataReference.currentGear = 1;
      vehicleDataReference.currentBreakPos = 200;
      vehicleDataReference.currentIgnitionOn = true;

      plc.setIP("127.0.0.1");
      plc.setPort(1502);
    }
    
    virtual void SetUp()
    {
      vehicleData.setSteeringEffort = vehicleDataReference.setSteeringEffort;
      vehicleData.setBreakEffort = vehicleDataReference.setBreakEffort;
      vehicleData.setThrottleEffort = vehicleDataReference.setThrottleEffort;
      vehicleData.setIgnitionOn = vehicleDataReference.setIgnitionOn;
      bool emergencyStop = vehicleDataReference.emergencyStop; // Using temporary to copy between boost::atomic<bool>
      vehicleData.emergencyStop = emergencyStop;
      vehicleData.setEngineStart = vehicleDataReference.setEngineStart;
      vehicleData.setSelectedGear = vehicleDataReference.setSelectedGear;
      plc.setSteeringAngleFeedback(vehicleDataReference.currentSteeringAngleRaw);
      plc.setCurrentlySelectedGear(vehicleDataReference.currentGear);
      plc.setCurrentBrakePosition(vehicleDataReference.currentBreakPos);
      plc.setIgnitionEnabled(vehicleDataReference.currentIgnitionOn);
    }

    static void TearDownTestCase()
    {
    }
};

OlavPLCSim PlcInterfaceTest::plc;
olav_controller::PlcInterface PlcInterfaceTest::plcInterface("127.0.0.1",1502,&PlcInterfaceTest::vehicleData);
VehicleData PlcInterfaceTest::vehicleData;
VehicleData PlcInterfaceTest::vehicleDataReference;

TEST_F(PlcInterfaceTest, plcWriteData)
{
  plc.start();
  plcInterface.open();
  plcInterface.writeData();
  EXPECT_EQ(vehicleDataReference.setSteeringEffort, -plc.getDesiredSteering());
  EXPECT_EQ(vehicleDataReference.setBreakEffort, plc.getDesiredBrake());
  EXPECT_EQ(vehicleDataReference.setThrottleEffort, plc.getDesiredThrottle());
  EXPECT_EQ(vehicleDataReference.setIgnitionOn, plc.getIgnitionEnable());
  EXPECT_EQ(vehicleDataReference.setIgnitionOn, !plc.getIgnitionDisable());
  EXPECT_EQ(vehicleDataReference.emergencyStop, plc.getEmergencyStop());
  EXPECT_EQ(vehicleDataReference.setEngineStart, plc.getEngineStart());
  EXPECT_EQ(vehicleDataReference.setSelectedGear, plc.getGearSelect());
  plcInterface.close();
  plc.stop();
}

TEST_F(PlcInterfaceTest, plcReadData)
{
  plc.start();
  plcInterface.open();
  plcInterface.readData();
  EXPECT_EQ(vehicleDataReference.currentSteeringAngleRaw, vehicleData.currentSteeringAngleRaw);
  EXPECT_EQ(vehicleDataReference.currentGear, vehicleData.currentGear);
  EXPECT_EQ(vehicleDataReference.currentBreakPos, vehicleData.currentBreakPos);
  EXPECT_EQ(vehicleDataReference.currentIgnitionOn, vehicleData.currentIgnitionOn);
  plcInterface.close();
  plc.stop();
}

TEST_F(PlcInterfaceTest, ticker)
{
  plc.start();
  plcInterface.open();
  plcInterface.writeData();
  bool prevTicker = plc.getKeepAliveTicker();
  plcInterface.writeData();
  EXPECT_EQ(plc.getKeepAliveTicker(), !prevTicker);
  plcInterface.close();
  plc.stop();
}

TEST_F(PlcInterfaceTest, isOpen)
{
  EXPECT_EQ(plcInterface.isOpen(),false);
  plc.start();
  plcInterface.open();
  EXPECT_EQ(plcInterface.isOpen(),true);
  plcInterface.writeData();
  plcInterface.close();
  plc.stop();
}

//vim: set expandtab sw=2 ts=2
