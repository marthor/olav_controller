#include <gtest/gtest.h>

#include <olav_simulator/arduino_sim.h>
#include "olav_controller/arduino_interface.h"
#include "olav_controller/vehicle.h"

class ArduinoInterfaceTest : public ::testing::Test
{
  protected:
    static ArduinoSim arduino;
    static olav_controller::ArduinoInterface arduinoInterface;
    static VehicleData vehicleData;

    static const std::string serialPortName;
    static const double wheelSpeed;
    static const double engineSpeed;
    static const bool estop;

    static void SetUpTestCase()
    {
      arduino.setVirtualSerialPortName(serialPortName);
      arduino.setWheelSpeed(wheelSpeed);
      arduino.setEngineSpeed(engineSpeed);
      arduino.setEmergencyStop(estop);
    }
    
    virtual void SetUp()
    {
    }

    static void TearDownTestCase()
    {
      arduino.stop();
    }
};

const std::string ArduinoInterfaceTest::serialPortName("/tmp/ttyTEST");
const double ArduinoInterfaceTest::wheelSpeed(1);
const double ArduinoInterfaceTest::engineSpeed(10);
const bool ArduinoInterfaceTest::estop(false);
ArduinoSim ArduinoInterfaceTest::arduino;
olav_controller::ArduinoInterface ArduinoInterfaceTest::arduinoInterface(ArduinoInterfaceTest::serialPortName, 9600, 10, &ArduinoInterfaceTest::vehicleData);
VehicleData ArduinoInterfaceTest::vehicleData;

TEST_F(ArduinoInterfaceTest, isOpen)
{
  arduino.start();
  EXPECT_EQ(false,arduinoInterface.isOpen());
  arduinoInterface.open();
  EXPECT_EQ(true,arduinoInterface.isOpen());
  arduino.stop();
  usleep(2000);
  EXPECT_EQ(false,arduinoInterface.isOpen());
  arduinoInterface.close();
  EXPECT_EQ(false,arduinoInterface.isOpen());
}

TEST_F(ArduinoInterfaceTest, read)
{
  arduino.start();
  arduinoInterface.open();
  while( !arduinoInterface.hasNewData() && arduinoInterface.isOpen() )
  {
  }
  if( !arduinoInterface.isOpen() )
  {
    FAIL() << "Arduino interface failed" << std::endl;
  }
  ASSERT_NEAR(vehicleData.currentAxelRadPrSec, wheelSpeed, 0.01);
  ASSERT_NEAR(vehicleData.getCurrentRpm(), engineSpeed*60/(2*M_PI), 1);
  EXPECT_EQ(estop,vehicleData.emergencyStop?true:false);
  arduinoInterface.close();
}

//vim: set expandtab sw=2 ts=2
