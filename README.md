Olav controller
=====================
ROS1 package for interfacing the Drive-by-wire kit of Olav, the UGV previously owned of FFI.
The DBW kit is controlled by a Wago PLC, and communication with the PLC is through the modbus protocol over an ethernet connection.
The *olav_controller* package has a ROS node that communicates with the PLC through modbus. 
It can control gear change, engine start/stop and emergency stop handling. 
It accepts driving commands of a custom type (*olav_msgs/vehicle_command*) based on the AckermannDrive message.

## Installation

### Pakker

Installer følgende:
```
sudo apt-get install ros-kinetic-ackermann-msgs
sudo apt-get install ros-kinetic-serial
sudo apt-get install ros-kinetic-control-toolbox
sudo apt-get install libmodbus-dev
git clone git@gitlabu.ffi.no:ugv-prosjekt/olav_msgs.git
```

### Oppsett av USB-portnavn
Se siste del av README om fast portnavn til Arduino.

### Tilgang til USB-porter

Skriv inn følgende:
```
sudo usermod -a -G dialout $USER 
```

## ROS2
*olav_controller* does not have ROS2 support. 
If you want to run it from ROS2, the [ros1_bridge](https://github.com/ros2/ros1_bridge) can be used to send messages from ROS2 to ROS1. 
This solution is untested for the `olav_controller` in particular though.
Is using Ubuntu22 or other some other OS without official support for ROS1, ROS1 can be run using [Robostack](https://robostack.github.io/index.html).

## Bruk

### På Olav

Start olav_controller:

`roslaunch olav_controller olav_real.launch`

Oppstartsrutine:
1. olav_controller starter i tilstanden "NO_STATE". Her vil den stå inntil den mottar vehicle_command-meldinger med kommandoen "VEHICLE_OFF".
2. Nå kan olav_controller kommanderes til tilstandene "VEHICLE_OFF", "VEHICLE_ON_PARK", "VEHICLE_ON_REVERSE", "VEHICLE_ON_LOW" og "VEHICLE_ON_HIGH".
3. Når "OK to drive"-flagget er true vil olav_controller akseptere hastighet- og svingkommandoer.

Kjekt å huske:
- servicen `/olav/start` må kalles for at OK to drive"-flagget skal være true, slik at Olav kan kjøre.
- dersom olav_controller har havnet i nødstopp, må denne fjernes med servicen `/olav/clear_emergency_stop` og brukeren må begynne på punkt 1.
- dersom Olav har vært i manuell modus (vha. autonom/manuell-bryteren), vil olav_controller gå til tilstanden "NO_STATE", og brukeren må begynne på punkt 1.

### Olav med hjul og belter

Det er laget egne launch filer for Olav med hjul og belter. Forskjellen på Olav med hjul og belter er hjulradiusen og maks styre vinkel.

#### Olav med hjul
Kjør følgende kommando:

`roslaunch olav_controller olav_real.launch use_belts:=false`

eller

`roslaunch olav_controller olav_real.launch` (use_belts er default false)

eller

`roslaunch olav_controller olav_real_wheels.launch`

#### Olav med belter
Kjør følgende kommando:

`roslaunch olav_controller olav_real.launch use_belts:=true`

eller

`roslaunch olav_controller olav_real_belts.launch`


## Nødstopp

olav_controller går til nødstopp dersom en punktene nedenfor inntreffer:
- servicen `/olav/emergency_stop` kalles
- olav_controller ikke mottar heartbeats på topicen `/olav/heart_beat`
- nødstoppbryter er trykket inn
- olav_controller har mistet kontakt med arduino
- olav_controller har mistet kontakt med PLS 
- Hastigheten er over 6 m/s

For å komme ut av nødstopp må servicen `/olav/clear_emergency_stop` kalles, olav_controller går da til "NO_STATE".

*Merk at olav_controller ikke går til nødstopp dersom olav_controller er i "NO_STATE" eller dersom autonom/manuell-modus bryteren står i manuell-modus.*

## OK to drive

For at olav_controller skal akseptere innholdet i `/olav/vehicle_command`-meldingene, må "OK to drive"-flagget være true. Flagget "OK to drive" er true når alle punktene nedenfor er tilfredsstilt:
- olav_controller IKKE er i nødstopp
- pls-en rapporterer at olav står i high, low eller reverse
- olav_controller mottar vehicle_command på topicen `/olav/vehicle_command`
- autonom/manuell-bryteren står i autonom-modus
- servicen `/olav/start` er kalt. Servicen må kalles på nytt etter at olav_controller har i tilstandene "NO_STATE", "EMERGENCY_STOP", "MANUAL_MODE" og "VEHICLE_OFF". (For mer info, se overskriften "Start- og stopp-service")

Når "OK to drive" er false, settes bremsene på.

"OK to drive"-flagget publiseres på topicen `/olav/vehicle_status`.

## Start- og stopp-service

For at Olav skal kjøre må servicen `/olav/start` kalles, hvis ikke vil "OK to drive" flagget være false, og Olav vil stå i ro med bremsene på. 
Tilsvarende vil Olav stoppe dersom servicen `olav/stop` kalles, og "OK to drive" flagget settes til false. Start-servicen må kalles på nytt etter at 
stopp-service har blitt kalt, eller dersom  olav_controller har vært i tilstandene "NO_STATE", "EMERGENCY_STOP", "MANUAL_MODE" og "VEHICLE_OFF".



## Grensesnitt mot olav_controller

### Innkommende meldinger

#### Kommandomeldinger

*Hvis olav_controller ikke mottar heartbeats, så vil olav_controller stoppe og stå stille med bremsene på. Dette vil markeres ved at ok_to_drive-flagget i vehicle_status-meldingen settes til false.*

Meldingsformat: `olav_msgs/vehicle_command.h`

Topic navn: `/olav/vehicle_command`

Timeout: 0.4 s (tilsvarer 10 tapte vehicle_command meldinger på 25 Hz)(valgfritt - spesifiseres i launch-fil)

#### Heartbeat

*Hvis olav_controller ikke mottar heartbeats, så vil olav_controller gå til nødstopp.*

Meldingsformat: `std_msgs/Header.h`

Topic navn: `/olav/heart_beat`

Timeout: 1.0 s (tilsvarer 10 tapte heart_beat meldinger på 10 Hz)(valgfritt - spesifiseres i launch-fil)

#### Navigasjon (for feedforward kontroller)

*Hvis olav_controller ikke mottar odometri-meldinger, så vil feedforward bidraget i hastighetskontrolleren bli satt til 0 og kontrolleren vil være en vanlig PID-kontroller.*

Meldingsformat: `nav_msgs/Odometry.h`

Topic navn: `/ugv_sensors/nav_ros/odometry_differential_ned`

Timeout: 1.0 s (tilsvarer 10 tapte odometry_differential_ned meldinger på 100 Hz)(valgfritt - spesifiseres i launch-fil)

### Utgående meldinger

#### linger

Meldingsformat: `ackermann_msgs/AckermannDriveStamped.h`

Topic navn: `/olav/vehicle_measurements`

Frekvens: 10 (valgfritt - spesifiseres i launch-fil)

#### Status

Meldingsformat: `olav_msgs/vehicle_status.h`

Topic navn: `/olav/vehicle_status`

Frekvens: 10 (valgfritt - spesifiseres i launch-fil)

#### Debug (Kun for debugformål. Skal ALDRI bagges sammen med dataopptak)

Meldingsformat: `olav_msgs/vehicle_debug.h`

Topic navn: `/olav/vehicle_debug`

Frekvens: 50 (valgfritt - spesifiseres i launch-fil)

### Service-er i olav_controller

#### Start å kjøre

Format: `std_srvs/Empty.h` 

Topic navn: `/olav/start`

#### Stopp å kjøre

Format: `std_srvs/Empty.h` 

Topic navn: `/olav/stop`

#### Nødstopp i olav_controller

Format: `std_srvs/Empty.h` 

Topic navn: `/olav/emergency_stop`

#### Fjerne nødstopp i olav_controller

Format: `std_srvs/Empty.h` 

Topic navn: `/olav/clear_emergency_stop`

### Service-er olav_controller kaller hos HAL

#### Nødstopp hos Mission Manager HAL

Format: `std_srvs/Empty.h`

Topic navn: `/hal_ugv/emergency_stop`

#### Fjerne nødstopp hos HAL

Format: `std_srvs/Empty.h`

Topic navn: `/hal_ugv/clear_emergency_stop`

#### Sette HAL i autonom/remote modus

Format: `std_srvs/Empty.h`

Topic navn: `/hal_ugv/set_hal_control_mode`

#### Sette HAL i manuell modus

Format: `std_srvs/Empty.h`

Topic navn: `/hal_ugv/set_manual_control_mode`


## Fast portnavn til Arduino
Det er opprettet en symlink til portnavnet til Arduinoen som olav_controlleren kommuniserer med.

### Oppsett av fast navn for USB-porter

Det ser ut til at portnavnene til usb-porter på Ubuntu er litt tilfeldig. En løsning på dette er å heller bruke en symlink til portadressen, slik at vi hele tiden kan koble oss til symlinken istedenfor å koble oss til portnavnet.

#### How-to 

First run `udevadm info /dev/ttyUSB0` and choose any identifying property (such as `ID_PATH` for the USB port). It's a good idea to always include the subsystem as well.

Then open (or create) a file in `/etc/udev/rules.d/` (named, for example, `serial-symlinks.rules`), and put the udev rule there.

For example, if the output for `ttyUSB0` is:
```
$ udevadm info /dev/ttyUSB0
. . .
E: ID_PATH=pci-0000:00:1d.0-usb-0:1.2:1.0
. . .
E: SUBSYSTEM=tty
. . .
```
...you can write this rule:

`SUBSYSTEM=="tty", ENV{ID_PATH}=="pci-0000:00:1d.0-usb-0:1.2:1.0", SYMLINK+="ttyOlavArduino"`

...and udev will always symlink `/dev/ttyOlavArduino` to whatever tty device you connected to USB port #2.

*Hentet fra: https://superuser.com/questions/536478/how-to-lock-device-ids-to-port-addresses*
