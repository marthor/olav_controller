#include "olav_controller/vehicle.h"
#include "olav_controller/VehicleSetupConfig.h"
#include <boost/algorithm/clamp.hpp>
#include <ros/ros.h>


Vehicle::Vehicle(int loopRate)
{
  this->loopRate = loopRate;

  // States
  internalState = NO_STATE;
  prevInternalState = NO_STATE;
  nextInternalState = NO_STATE;


  //Init values
  plcOpenLast = false;
  loopCounter = 0;
  reconnectCounter = loopRate;
  odometry_timeout = 1;
  olavCommand.inclination = 0;
  firstMsg = true;

  setupVehicle();
}


Vehicle::~Vehicle()
{
  if (plcInterfacePtr != nullptr)
    delete plcInterfacePtr;

  if (feedforwardSpeedController != nullptr)
    delete feedforwardSpeedController;

  if (arduinoInterface != nullptr)
    delete arduinoInterface;
}


void Vehicle::setupVehicle()
{
  //Olav Setup
  ros::NodeHandle privateNH("~");

  // Setting up SpeedController PID
  ros::NodeHandle pid_nh("~/speed_controller");
  speedControllerPID.init(pid_nh);
  speedControllerPID.reset();

  // Set up feed forward controller
  ros::NodeHandle feedforwardSpeedController_nh("~/feedforward_speed_controller");
  feedforwardSpeedController = new olav_controller::FeedforwardSpeedController(feedforwardSpeedController_nh);

  //Setting up SteeringController PID:
  ros::NodeHandle pid_steering_nh("~/steering_controller");
  steeringControllerPID.init(pid_steering_nh);
  steeringControllerPID.reset();

  // Start Arduino Communication
  std::string arduinoPort;
  int arduinoBaudrate = 115200;
  int arduinoTimeout = 1000;
  privateNH.getParam("Arduino/device", arduinoPort);
  //privateNH.getParam("Arduino/baudrate", arduinoBaudrate);
  //privateNH.getParam("Arduino/timeout", arduinoTimeout);
  ROS_INFO_STREAM("Setting up Arduino interface: " << arduinoPort << "@" << arduinoBaudrate << "baud");
  arduinoInterface = new ArduinoInterface(arduinoPort, arduinoBaudrate, arduinoTimeout, &olavMeasurements);
  try
  {
    arduinoInterface->open();
    ROS_INFO_STREAM("Arduino interface opened");
  } catch (ArduinoInterfaceException& e)
  {
    ROS_ERROR("Failed to open Arduino interface: %s", e.what());
  }

  //PLC Setup
  std::string plcAddress = "";
  int plcPort = 0;

  privateNH.getParam("PLC/ip_address", plcAddress);
  privateNH.getParam("PLC/port_number", plcPort);
  ROS_INFO_STREAM("Setting up PLC interface: " << plcAddress << ":" << plcPort);
  plcInterfacePtr = new PlcInterface(plcAddress, plcPort, &olavMeasurements);

  // Initialize Olav
  double wheel_radius;
  privateNH.getParam("vehicle_setup/wheel_radius", wheel_radius);
  olavMeasurements.setWheelCircumferenceM(2 * M_PI * wheel_radius);

  double publish_vehicle_measurements_rate;
  privateNH.getParam("publish/vehicle_measurements_rate", publish_vehicle_measurements_rate);
  setAckermannDriveAverageSize(static_cast<int>(publish_vehicle_measurements_rate));

  privateNH.getParam("subscribe/odometry_timeout", odometry_timeout);
}


void Vehicle::updateInternalState(VehicleStates externalState)
{
  if (loopCounter > 0)
    loopCounter--;

  switch (internalState)
  {
    case NO_STATE:
      olavCommand.ignitionOn = false;

      prevInternalState = NO_STATE;
      if (externalState == VEHICLE_OFF)
      {
        internalState = IGNITION_OFF;
      }
      break;
    case IGNITION_OFF:
      olavCommand.ignitionOn = false;

      prevInternalState = IGNITION_OFF;
      if (externalState == VEHICLE_ON_PARK ||
          externalState == VEHICLE_ON_REVERSE ||
          externalState == VEHICLE_ON_LOW ||
          externalState == VEHICLE_ON_HIGH)
      {
        internalState = IGNITION_ON_ENGINE_OFF;
      }
      break;
    case IGNITION_ON_ENGINE_OFF:
      if (prevInternalState != internalState)
      {
        prevInternalState = internalState;
        olavCommand.ignitionOn = true;
        olavCommand.gear = PARK;

        loopCounter = loopRate * 3;
      }
      if (externalState == VEHICLE_ON_PARK ||
          externalState == VEHICLE_ON_REVERSE ||
          externalState == VEHICLE_ON_LOW ||
          externalState == VEHICLE_ON_HIGH)
      {
        if (olavMeasurements.isIgnitionOn && loopCounter == 0)
        {
          internalState = IGNITION_ON_ENGINE_STARTING;
        }
      }
      else
      {
        internalState = IGNITION_OFF;
      }
      break;
    case IGNITION_ON_ENGINE_STARTING:
      if (prevInternalState != internalState)
      {
        prevInternalState = internalState;
        olavCommand.starterEngineOn = true;
        olavCommand.gear = PARK;
        loopCounter = loopRate * 2;
      }
      if (externalState == VEHICLE_ON_PARK ||
          externalState == VEHICLE_ON_REVERSE ||
          externalState == VEHICLE_ON_LOW ||
          externalState == VEHICLE_ON_HIGH)
      {
        if (olavMeasurements.rpm > 800)
        {
          olavCommand.starterEngineOn = false;
          internalState = ENGINE_ON_PARK;
        }
        else if (loopCounter == 0)
        {
          internalState = IGNITION_OFF;
        }
      }
      else
      {
        internalState = IGNITION_OFF;
      }
      break;
    case ENGINE_ON_PARK:
      prevInternalState = internalState;
      if (externalState == VEHICLE_ON_REVERSE ||
          externalState == VEHICLE_ON_LOW ||
          externalState == VEHICLE_ON_HIGH)
      {
        nextInternalState = translateGearExternalStates(externalState);
        internalState = ENGINE_ON_CHANGING_GEAR;
      }
      else if (externalState == VEHICLE_ON_PARK)
      {
        if (olavMeasurements.gear != PARK)
        {
          nextInternalState = translateGearExternalStates(externalState);
          internalState = ENGINE_ON_CHANGING_GEAR;
        }
      }
      else if (externalState == VEHICLE_OFF)
      {
        internalState = IGNITION_OFF;
      }
      else
      {
        //externalState is MANUAL_MODE, EMERGENCY_STOP or NO_STATE
      }
      break;
    case ENGINE_ON_NEUTRAL:
      prevInternalState = internalState;
      if (externalState == VEHICLE_ON_REVERSE ||
          externalState == VEHICLE_ON_LOW ||
          externalState == VEHICLE_ON_PARK ||
          externalState == VEHICLE_ON_HIGH)
      {
        nextInternalState = translateGearExternalStates(externalState);
        internalState = ENGINE_ON_CHANGING_GEAR;
      }
      else if (externalState == VEHICLE_OFF)
      {
        internalState = IGNITION_OFF;
      }
      else
      {
        //externalState is MANUAL_MODE, EMERGENCY_STOP or NO_STATE
      }
      break;
    case ENGINE_ON_REVERSE:
      prevInternalState = internalState;
      if (externalState == VEHICLE_ON_PARK ||
          externalState == VEHICLE_ON_LOW ||
          externalState == VEHICLE_ON_HIGH)
      {
        nextInternalState = translateGearExternalStates(externalState);
        internalState = ENGINE_ON_CHANGING_GEAR;
      }
      else if (externalState == VEHICLE_ON_REVERSE)
      {
        if (olavMeasurements.gear != REVERSE || !isInGear())
        {
          nextInternalState = translateGearExternalStates(externalState);
          internalState = ENGINE_ON_CHANGING_GEAR;
        }
      }
      else if (externalState == VEHICLE_OFF)
      {
        internalState = IGNITION_OFF;
      }
      else
      {
        //externalState is MANUAL_MODE, EMERGENCY_STOP or NO_STATE
      }
      break;
    case ENGINE_ON_LOW:
      prevInternalState = internalState;
      if (externalState == VEHICLE_ON_REVERSE ||
          externalState == VEHICLE_ON_PARK ||
          externalState == VEHICLE_ON_HIGH)
      {
        nextInternalState = translateGearExternalStates(externalState);
        internalState = ENGINE_ON_CHANGING_GEAR;
      }
      else if (externalState == VEHICLE_ON_LOW)
      {
        if (olavMeasurements.gear != LOW || !isInGear())
        {
          nextInternalState = translateGearExternalStates(externalState);
          internalState = ENGINE_ON_CHANGING_GEAR;
        }
      }
      else if (externalState == VEHICLE_OFF)
      {
        internalState = IGNITION_OFF;
      }
      else
      {
        //externalState is MANUAL_MODE, EMERGENCY_STOP or NO_STATE
      }
      break;
    case ENGINE_ON_HIGH:
      prevInternalState = internalState;
      if (externalState == VEHICLE_ON_REVERSE ||
          externalState == VEHICLE_ON_LOW ||
          externalState == VEHICLE_ON_PARK)
      {
        nextInternalState = translateGearExternalStates(externalState);
        internalState = ENGINE_ON_CHANGING_GEAR;
      }
      else if (externalState == VEHICLE_ON_HIGH || !isInGear())
      {
        if (olavMeasurements.gear != HIGH)
        {
          nextInternalState = translateGearExternalStates(externalState);
          internalState = ENGINE_ON_CHANGING_GEAR;
        }
      }
      else if (externalState == VEHICLE_OFF)
      {
        internalState = IGNITION_OFF;
      }
      else
      {
        //externalState is MANUAL_MODE, EMERGENCY_STOP or NO_STATE
      }
      break;
    case ENGINE_ON_CHANGING_GEAR:
      prevInternalState = internalState;

      if (olavMeasurements.getSpeedMs() < olavCommand.minCommandedSpeedBeforeBraking)
      {
        switch (nextInternalState)
        {
          case ENGINE_ON_PARK:
            olavCommand.gear = PARK;
            break;
          case ENGINE_ON_REVERSE:
            olavCommand.gear = REVERSE;
            break;
          case ENGINE_ON_LOW:
            olavCommand.gear = LOW;
            break;
          case ENGINE_ON_HIGH:
            olavCommand.gear = HIGH;
            break;
          default:
            break;
        }

        if (olavMeasurements.gear == olavCommand.gear && olavMeasurements.isGearInPosition)
        {
          if (externalState == VEHICLE_ON_PARK)
          {
            internalState = ENGINE_ON_PARK;
          }
          else
          {
            loopCounter = 10;
            gear_shift_throttle_ = 10;
            internalState = ENGINE_ON_ENGAGING_GEAR;
          }
        }
      }
      break;
    case ENGINE_ON_ENGAGING_GEAR:

      olavCommand.breakEffort = 80;
      olavCommand.throttleEffort = gear_shift_throttle_;

      if (olavMeasurements.rpm > 1700)
      {

        if (loopCounter == 0)
        {
          gear_shift_throttle_ = 0;
          loopCounter = loopRate;
          olavCommand.throttleEffort = 0;
          internalState = nextInternalState;
        }
      }
      else
      {
        loopCounter = 10;
        gear_shift_throttle_ += 1;
      }
      break;
    default:
      ROS_ERROR("InternalStateMachine entered default case. Going to IGNITION_OFF");
      internalState = IGNITION_OFF;
      break;
  }
}


Vehicle::InternalVehicleStates Vehicle::translateGearExternalStates(VehicleStates externalState)
{
  switch (externalState)
  {
    case VEHICLE_ON_PARK:
      return ENGINE_ON_PARK;
    case VEHICLE_ON_REVERSE:
      return ENGINE_ON_REVERSE;
    case VEHICLE_ON_LOW:
      return ENGINE_ON_LOW;
    case VEHICLE_ON_HIGH:
      return ENGINE_ON_HIGH;
    default:
    {
      ROS_ERROR("Error in %s, Cannot translate %d, going to IGNITION_OFF ", __FUNCTION__, externalState);
      return IGNITION_OFF;
    }
  }
}


bool Vehicle::arduinoIsOK()
{
  if (arduinoInterface->isReceivingData())
    return true;
  else
    return false;
}


bool Vehicle::modbusIsOK()
{
  return plcInterfacePtr->isOpen();
}


bool Vehicle::isReceivingOdometry()
{
  time_since_last_odometry_ = (ros::Time::now() - timeLastOdometryMsg).toSec();

  if (time_since_last_odometry_ < odometry_timeout)
    return true;
  else
  {
//    ROS_WARN_THROTTLE(30, "Receiving Odometry failed (dt = %.2f sec > %.2f sec)", time_since_last_odometry_, odometry_timeout);
    return false;
  }
}


void Vehicle::setVehicleState(VehicleStates vehicleState)
{
  this->vehicleState = vehicleState;
}


void Vehicle::breaksOnAndReset(int breakEffort) //Default breakEffort = 80
{
  resetControllers();

  olavCommand.breakEffort = breakEffort;
}


void Vehicle::resetControllers()
{
  speedControllerPID.reset();
  steeringControllerPID.reset();

  olavCommand.clearSpeedControllerVariables();
  olavCommand.clearSteeringControllerVariables();
}


void Vehicle::setTargets(double speedMs, double accelMs2, double steeringRad, double steeringVelRadPrS)
{
  ros::Time timeNow = ros::Time::now();
//  std::chrono::time_point<std::chrono::system_clock> timeNow = std::chrono::system_clock::now();

  if (olavCommand.timeLast.toSec() > 0)
    olavCommand.dt = timeNow - olavCommand.timeLast;
  else
    olavCommand.dt.fromSec(0);

  olavCommand.timeLast = timeNow;

//  ROS_INFO("ROS: %.6f",olavCommand.dt.toSec() );

  // Clamp targets
  olavCommand.targetSpeed = std::min(speedMs, olavCommand.maxCommandedSpeed);
  olavCommand.targetAcceleration = accelMs2 == 0.0 ? olavCommand.maxCommandedAcceleration : std::min(accelMs2, olavCommand.maxCommandedAcceleration);
  olavCommand.setTargetSteeringAngleRad(boost::algorithm::clamp(steeringRad, -olavCommand.getMaxCommandedSteeringAngleRad(), olavCommand.getMaxCommandedSteeringAngleRad()));
  olavCommand.setTargetSteeringVelocityRadPrSec(steeringVelRadPrS == 0.0 ? olavCommand.getMaxCommandedSteeringVelocityRadPrSec() : std::min(steeringVelRadPrS, olavCommand.getMaxCommandedSteeringVelocityRadPrSec()));

  // Speed controller:
  if (olavCommand.targetSpeed <= olavCommand.minCommandedSpeedBeforeBraking)
  {
    breaksOnAndReset(80);
  }
  else
  {
    // Ramp speed set point to limit acceleration
    if (olavCommand.targetSpeed > olavCommand.setpointSpeed)
      olavCommand.setpointSpeed = rampSetPoint(olavCommand.setpointSpeed, olavCommand.targetSpeed,
                                               olavCommand.targetAcceleration, olavCommand.dt.toSec());
    else
      olavCommand.setpointSpeed = olavCommand.targetSpeed;

    // Feed forward component
    if (feedforwardSpeedController->getEnableFeedforward() && isReceivingOdometry())
    {
      olavCommand.useFeedForward = true;

      // Filter out inclination from pitch using a low pass filter
      olavCommand.inclination += olavCommand.dt.toSec() / 0.2 * (olavMeasurements.pitch - olavCommand.inclination);

      // Calculate feed forward throttle
      olavCommand.feedforwardEffort = feedforwardSpeedController->computeCommand(olavCommand.setpointSpeed,
                                                                                 olavMeasurements.gear,
                                                                                 olavCommand.inclination);
    }
    else
    {
      olavCommand.useFeedForward = false;

      //ROS_DEBUG_THROTTLE(30, "Not using feedforward because no/old odometry(pitch) data");
      olavCommand.inclination = 0;
      olavCommand.feedforwardEffort = 0;
    }

    // Calculate PID
    olavCommand.speedError = olavCommand.setpointSpeed - olavMeasurements.getSpeedMs();
    olavCommand.pidThrottleEffort = speedControllerPID.computeCommand(olavCommand.speedError, olavCommand.dt);

    // Limit throttle command
    olavCommand.throttleEffort = boost::algorithm::clamp(olavCommand.pidThrottleEffort + olavCommand.feedforwardEffort,
                                                         -80.0, olavCommand.maxCommandedThrottleEffort);

    if (olavCommand.throttleEffort < 0)
    {
      olavCommand.breakEffort = std::abs(olavCommand.throttleEffort);
      olavCommand.throttleEffort = 0;
    }
    else
    {
      olavCommand.breakEffort = 0;
    }
  }


  // Constrain and ramp steering angle set point
  olavCommand.setSetpointSteeringAngleDeg(
      rampSetPoint(olavCommand.getSetpointSteeringAngleDeg(), olavCommand.getTargetSteeringAngleDeg(),
                   olavCommand.getMaxCommandedSteeringVelocityDegPrSec(), olavCommand.dt.toSec()));;

  olavCommand.steeringAngleErrorDeg = olavCommand.getSetpointSteeringAngleDeg() - olavMeasurements.getSteeringAngleDeg();
  olavCommand.pidSteeringEffort = steeringControllerPID.computeCommand(olavCommand.steeringAngleErrorDeg, olavCommand.dt);
  olavCommand.steeringEffort = boost::algorithm::clamp(olavCommand.pidSteeringEffort,
                                                       -olavCommand.maxCommandedSteeringEffort,
                                                       olavCommand.maxCommandedSteeringEffort);
}


Vehicle::InternalVehicleStates Vehicle::getCurrentVehicleState()
{
  return internalState;
}

Vehicle::InternalVehicleStates Vehicle::getPrevVehicleState()
{
  return prevInternalState;
}

Vehicle::InternalVehicleStates Vehicle::getNextVehicleState()
{
  return nextInternalState;
}


void Vehicle::emergencyStop()
{
  breaksOnAndReset(100);
  internalState = Vehicle::IGNITION_OFF;
}


bool Vehicle::isEmergencyStop()
{
  return olavMeasurements.isEmergencyStop;
}


bool Vehicle::isManualMode()
{
  return !olavMeasurements.isAutonomousModeOn;
}


bool Vehicle::isAutonomousMode()
{
  return olavMeasurements.isAutonomousModeOn;
}


void Vehicle::updatePLC()
{
//  std::cout << "---------------------\n" << __FUNCTION__ << "\n---------------------" << std::endl;
//  std::cout << "plcInterfacePtr->isOpen(): " << plcInterfacePtr->isOpen() << std::endl;

  if (plcInterfacePtr->isOpen())
  {
    if (!plcOpenLast)
    {
      plcOpenLast = true;
      ROS_INFO("Successfully connected to the plc");
    }

    try
    {
      plcInterfacePtr->writeData(olavCommand.steeringEffort, olavCommand.breakEffort, olavCommand.throttleEffort,
                                 olavCommand.ignitionOn, false, olavCommand.starterEngineOn, olavCommand.gear);
      plcInterfacePtr->readData();
    }
    catch (PlcInterfaceException e)
    {
      plcOpenLast = false;
      ROS_WARN("Lost connection with the plc, trying to reconnect...");
    }

  }
  else
  {
    internalState = NO_STATE;

    if (reconnectCounter >= loopRate)
    {
      try
      {
        plcInterfacePtr->open();
        reconnectCounter = 0;
      } catch (PlcInterfaceException e)
      {
        ROS_WARN_THROTTLE(1, "Failed to connect to the plc, trying to reconnect...");
      }
    }
    else
    {
      reconnectCounter++;
    }
  }
}


double Vehicle::rampSetPoint(double setPoint, double targetSetPoint, double rate, double dt)
{
  if (rate == 0.0)
  {
    // set point = target set point if rate limiting disabled.
    return targetSetPoint;
  }
  else
  {
    // Move towards target set point with speed = dt*rate
    return setPoint + boost::algorithm::clamp(targetSetPoint - setPoint, -dt * rate, dt * rate);
  }
}


void Vehicle::updateMeasurements()
{
  // Calculate ackermann_drive_current
  ackermann_drive_current.header.stamp = ros::Time::now();
  ackermann_drive_current.drive.speed = olavMeasurements.getSpeedMs();
  ackermann_drive_current.drive.steering_angle = olavMeasurements.getSteeringAngleRad();

  double dt = ackermann_drive_current.header.stamp.toSec() - ackermann_drive_last.header.stamp.toSec();
  if (!firstMsg && (dt > 1e-6))
  {
    ackermann_drive_current.drive.acceleration =
        (ackermann_drive_current.drive.speed - ackermann_drive_last.drive.speed) / dt;
    ackermann_drive_current.drive.jerk =
        (ackermann_drive_current.drive.acceleration - ackermann_drive_last.drive.acceleration) / dt;
    ackermann_drive_current.drive.steering_angle_velocity =
        (ackermann_drive_current.drive.steering_angle_velocity - ackermann_drive_last.drive.steering_angle_velocity) /
        dt;
  }
  else
  {
    ackermann_drive_current.drive.acceleration = 0;
    ackermann_drive_current.drive.jerk = 0;
    ackermann_drive_current.drive.steering_angle_velocity = 0;
  }

  // Calculate ackermann_drive_average
  ackermann_drive_average.header = ackermann_drive_current.header;
  ackermann_drive_average.drive.speed = averageSpeed.getMovingAverage(ackermann_drive_current.drive.speed);
  ackermann_drive_average.drive.steering_angle = averageSteeringAngle.getMovingAverage(
      ackermann_drive_current.drive.steering_angle);

  if (!firstMsg && dt > 1e-6)
  {
    ackermann_drive_current.drive.acceleration = averageAcceleration.getMovingAverage(
        ackermann_drive_current.drive.acceleration);
    ackermann_drive_current.drive.jerk = averageJerk.getMovingAverage(ackermann_drive_current.drive.jerk);
    ackermann_drive_current.drive.steering_angle_velocity = averageSteeringAngleVelocity.getMovingAverage(
        ackermann_drive_current.drive.steering_angle_velocity);
  }
  else
  {
    ackermann_drive_current.drive.acceleration = ackermann_drive_current.drive.acceleration;
    ackermann_drive_current.drive.jerk = ackermann_drive_current.drive.jerk;
    ackermann_drive_current.drive.steering_angle_velocity = ackermann_drive_current.drive.steering_angle_velocity;
  }

  firstMsg = false;
}


bool Vehicle::isInGear()
{
  if (olavMeasurements.rpm >= olavCommand.maxRpmInGearChange && olavMeasurements.getSpeedMs() <= olavCommand.minCommandedSpeedBeforeBraking)
  {
    ROS_ERROR("Failed to change gear. Going to change gear routine!");
    return false;
  }
  else
  {
    return true;
  }
}


ackermann_msgs::AckermannDrive Vehicle::getAckermannDriveAverage()
{
  return ackermann_drive_average.drive;
}


ackermann_msgs::AckermannDrive Vehicle::getAckermannDriveFullRate()
{
  return ackermann_drive_current.drive;
}


void Vehicle::setAckermannDriveAverageSize(const int& rate)
{
  averageSteeringAngle.setFilterSize(static_cast<size_t>(loopRate / rate));
  averageSteeringAngleVelocity.setFilterSize(static_cast<size_t>(loopRate / rate));
  averageSpeed.setFilterSize(static_cast<size_t>(loopRate / rate));
  averageAcceleration.setFilterSize(static_cast<size_t>(loopRate / rate));
  averageJerk.setFilterSize(static_cast<size_t>(loopRate / rate));
}
