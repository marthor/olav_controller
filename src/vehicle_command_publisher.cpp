#include "ros/ros.h"
#include "olav_msgs/vehicle_command.h"


olav_msgs::vehicle_command vehicle_command;

void chatterCallback(const olav_msgs::vehicle_command::ConstPtr& msg)
{
  ROS_INFO_THROTTLE(1,"Receiving...");
  vehicle_command = *msg;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "vehicle_command_publisher");
  ros::NodeHandle n;

  std::string topic_pub = "/ugv_control/vehicle_command";
  std::string topic_sub = "/olav/vehicle_keyboard_command";

  ros::Publisher pub = n.advertise<olav_msgs::vehicle_command>(topic_pub, 1000);
  ros::Subscriber sub = n.subscribe(topic_sub, 1000, chatterCallback);
  ros::Rate loop_rate(10);

  ROS_INFO_STREAM("Subscribing on " << topic_sub);
  ROS_INFO_STREAM("Publishing on " << topic_pub);

  while (ros::ok())
  {
    ROS_INFO_THROTTLE(1,"Publishing...");

    vehicle_command.header.stamp = ros::Time::now();
    pub.publish(vehicle_command);
    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}
