#include <sstream>
#include "olav_controller/arduino_interface.h"

ArduinoInterface::ArduinoInterface(std::string serialPortName, uint32_t baudrate, uint32_t timeout,
                                   VehicleMeasurements* olavMeasurements)
    :
    runWorker(false),
    workerStatus(stopped),
    newData(false)
{
  this->serialPort.setPort(serialPortName);
  this->serialPort.setBaudrate(baudrate);
  serial::Timeout timeoutStruct = serial::Timeout::simpleTimeout(timeout);
  this->serialPort.setTimeout(timeoutStruct);
  this->olavMeasurements = olavMeasurements;
  lastMsg = std::chrono::system_clock::now();
}


ArduinoInterface::~ArduinoInterface()
{
  this->close();
}


void ArduinoInterface::open()
{
  // Throw an exception if connection is already open
  if (this->isOpen())
  {
    throw ArduinoInterfaceException("Connection to Arduino already open.");
  }

  // If worker thread is active but disconnected: close it
  if (this->workerStatus == disconnected)
  {
    this->close();
  }

  // Try to open connection to Arduino
  try
  {
    // Open port and flush buffer
    this->serialPort.open();
    this->serialPort.flush();

    // Try to set data ready
    try
    {
      this->serialPort.setDTR(true);
    }
    catch (serial::SerialException& e)
    {
      // DTR is not supported on psudo terminal used dur>ing simulations resulting in an SerialException.
    }

    // Start thread reading from serial
    this->runWorker = true;
    workerThread = boost::thread(&ArduinoInterface::worker, this);
    while (workerStatus == stopped)
    {
    }
  }
  catch (serial::IOException& e)
  {
    std::stringstream ss;
    ss << "Failed to open serial connection to Arduino: " << e.what();
    throw ArduinoInterfaceException(ss.str().c_str());
  }
}


void ArduinoInterface::close()
{
  if (this->workerStatus != stopped)
  {
    this->runWorker = false;
    this->workerThread.join();
    this->serialPort.close();
    this->workerStatus = stopped;
  }
}


bool ArduinoInterface::isOpen()
{
  if (this->workerStatus == connected)
  {
    return true;
  }
  return false;
}


bool ArduinoInterface::hasNewData()
{
  if (this->newData)
  {
    this->newData = false;
    return true;
  }
  return false;
}


void ArduinoInterface::worker()
{
  int estop = 0;
  int centiDegPrSec = 0;
  int rpm = 0;
  while (runWorker && serialPort.isOpen())
  {
    this->workerStatus = connected;
    try
    {
      // Read one line or until timeout
      std::string data = serialPort.readline();

      // If no line was read: stop worker
      if (data.empty())
      {
        this->runWorker = false;
      }
        // Look for UGV header
      else if (data.find("UGV:") == 0)
      {
        // Extract data
        int tmp = data.find_first_of(":") + 1;
        centiDegPrSec = std::atoi(data.substr(tmp).c_str());
        tmp = data.find_first_of(",") + 1;
        rpm = std::atoi(data.substr(tmp).c_str());
        tmp = data.find_first_of(",", tmp) + 1;
        estop = std::atoi(data.substr(tmp).c_str());

        olavMeasurements->setSpeedCentiDegPrSec(centiDegPrSec);
        olavMeasurements->rpm = rpm;
        olavMeasurements->isEmergencyStop = estop == 0;

        this->newData = true;
        lastMsg = std::chrono::system_clock::now();
      }
    }
    catch (...)
    {
      this->runWorker = false;
    }
  }

  this->workerStatus = disconnected;
}

bool ArduinoInterface::isReceivingData()
{
  if (this->isOpen())
  {
    long timeMs = std::chrono::duration_cast<std::chrono::milliseconds>(
        std::chrono::system_clock::now() - lastMsg).count();
    if (timeMs < 200)
      return true;
  }

  return false;
}
