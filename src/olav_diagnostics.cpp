#include <olav_controller/olav_diagnostics.h>


OlavDiagnostics::OlavDiagnostics(diagnostic_updater::Updater& diag_updater)
{
  diag_updater.add("Vehicle status", this, &OlavDiagnostics::diagnosticsCallbackVehicleStatus);
}


void OlavDiagnostics::updateVehicleState(const VehicleStates& state,
                                         const VehicleStates& next_state,
                                         const VehicleStates& prev_state)
{
  state_ = state;
  next_state_ = next_state;
  prev_state_ = prev_state;
}

void OlavDiagnostics::updateInternalVehicleStates(const Vehicle::InternalVehicleStates& state,
                                                  const Vehicle::InternalVehicleStates& next_state,
                                                  const Vehicle::InternalVehicleStates& prev_state)
{
  internal_state_ = state;
  next_internal_state_ = next_state;
  prev_internal_state_ = prev_state;
}


void OlavDiagnostics::updateVehicleMeasurements(const bool& isIgnitionOn,
                                                const double& rpm,
                                                const Gears& gear,
                                                const double& speedMs)
{
  isIgnitionOn_ = isIgnitionOn;
  rpm_ = rpm;
  gear_ = gear;
  speedMs_ = speedMs;
}


void OlavDiagnostics::diagnosticsCallbackVehicleStatus(diagnostic_updater::DiagnosticStatusWrapper& status)
{
  if (is_emergency_stop_)
  {
    status.summary(diagnostic_msgs::DiagnosticStatus::ERROR, "Emergency stop.");
  } else if (!is_ok_to_drive_ && !is_emergency_stop_)
  {
    status.summary(diagnostic_msgs::DiagnosticStatus::WARN, "Some of the start conditions is not present");
  } else
  {
    status.summary(diagnostic_msgs::DiagnosticStatus::OK, "Everything is ok");
  }


  status.add("Vehicle state", getVehicleStatesString(state_));
  status.add("Internal vehicle state", getInternalVehicleStatesString(internal_state_));
  status.add("", "");
  status.add("Mode", is_autonomous_mode_ ? "Autonomous" : "Manual");
  status.add("", "");
  status.add("Emergency stop", is_emergency_stop_ ? "Yes" : "No");
  status.add("Arduino connection", is_arduino_ok_ ? "Ok" : "No connection");
  status.add("Modbus connection", is_modbus_ok_ ? "Ok" : "No connection");
  status.add("Emergency stop button", is_emergency_stop_button_pressed_ ? "Emergency stop" : "Ok");
  status.add("Emergency stop service", is_emergency_stop_service_called_ ? "Emergency stop" : "Ok");
  status.add("Speed restriction", is_speed_ok_ ? "Ok" : "Speed is too high");
  status.add("", "");
  status.add("Ok to drive", is_ok_to_drive_ ? "Yes" : "No");
  status.add("Start service called", is_external_ok_to_drive_ ? "Yes" : "No");
  status.add("Autonomous mode", is_autonomous_mode_ ? "True" : "False");
  status.add("Drivable gear", is_drivable_gear_ ? "Yes" : "No");
  status.add("Receiving vehicle command", is_receiving_vehicle_commands_ ? "Ok" : "Not receiving (Time since last message: " + std::to_string(time_since_last_vehicle_command_) + " s)");
  status.add("Receiving heartbeats", is_receiving_hearbeats_ ? "Ok" : "Not receiving (Time since last message: " + std::to_string(time_since_last_heartbeat_) + " s)");
  status.add("Receiving odometry messages", is_receiving_odometry_messages_ ? "Ok" : "Not receiving (Time since last message: " + std::to_string(time_since_last_odometry_message_) + " s)");
  status.add("", "");
  status.add("Ignition on", isIgnitionOn_ ? "True" : "False");
  status.add("Current speed [ms]", speedMs_);
  status.add("Current gear", getVehicleGearsString(gear_));
  status.add("Current RPM", rpm_ < 0.1 ? 0 : rpm_);
}


std::string OlavDiagnostics::getVehicleStatesString(VehicleStates state)
{
  switch (state)
  {
    case NO_STATE:
      return "No State";
    case EMERGENCY_STOP:
      return "Emergency Stop";
    case MANUAL_MODE:
      return "Manual Mode";
    case VEHICLE_OFF:
      return "Vehicle Off";
    case VEHICLE_ON_PARK:
      return "Vehicle On Park";
    case VEHICLE_ON_REVERSE:
      return "Vehicle On Reverse";
    case VEHICLE_ON_LOW:
      return "Vehicle On Low";
    case VEHICLE_ON_HIGH:
      return "Vehicle On High";
    default:
      return "Unknown State";
  }
}


std::string OlavDiagnostics::getInternalVehicleStatesString(Vehicle::InternalVehicleStates state)
{
  switch (state)
  {
    case Vehicle::NO_STATE:
      return "No State";
    case Vehicle::IGNITION_OFF:
      return "Ignition Off";
    case Vehicle::IGNITION_ON_ENGINE_OFF:
      return "Ignition On Engine Off";
    case Vehicle::IGNITION_ON_ENGINE_STARTING:
      return "Ignition On Engine Starting";
    case Vehicle::ENGINE_ON_PARK:
      return "Engine On Park";
    case Vehicle::ENGINE_ON_NEUTRAL:
      return "Engine On Neutral";
    case Vehicle::ENGINE_ON_REVERSE:
      return "Engine On Reverse";
    case Vehicle::ENGINE_ON_LOW:
      return "Engine On Low";
    case Vehicle::ENGINE_ON_HIGH:
      return "Engine On High";
    case Vehicle::ENGINE_ON_CHANGING_GEAR:
      return "Engine On Changing Gear";
    case Vehicle::ENGINE_ON_ENGAGING_GEAR :
      return "Engine On Engaging Gear";
    default:
      return "Unknown State";
  }
}


std::string OlavDiagnostics::getVehicleGearsString(Gears gear)
{
  switch (gear)
  {
    case PARK:
      return "Park";
    case REVERSE:
      return "Reverse";
    case NEUTRAL:
      return "Neutral";
    case LOW:
      return "Low";
    case HIGH:
      return "High";
    default:
      return "Unknown Gear";
  }
}