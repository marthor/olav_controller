#include <ros/ros.h>
#include <iostream>
#include <boost/atomic.hpp>
#include <control_toolbox/pid.h>
#include <std_msgs/Header.h>
#include <std_srvs/Empty.h>
#include <tf/transform_datatypes.h>
#include <diagnostic_updater/diagnostic_updater.h>
#include <diagnostic_updater/publisher.h>
#include "olav_controller/vehicle.h"
#include "olav_controller/vehicle_states.h"
#include "olav_controller/VehicleSetupConfig.h"
#include "olav_controller/olav_diagnostics.h"
#include "olav_controller/moving_average_filter.h"
#include <ackermann_msgs/AckermannDrive.h>
#include <ackermann_msgs/AckermannDriveStamped.h>
#include <nav_msgs/Odometry.h>
#include <olav_msgs/vehicle_command.h>
#include <olav_msgs/vehicle_debug.h>
#include <olav_msgs/vehicle_state.h>
#include <olav_msgs/vehicle_status.h>
#include <cmath>


const int LOOP_RATE = 50;
const int THROTTLE_RATE = 2;


Vehicle* olav = nullptr;
OlavDiagnostics* olav_diagnostics = nullptr;
olav_msgs::vehicle_command currentVehicleCommand;

ros::Time time_last_heartbeat_msg(0);
ros::Time time_last_vehicle_command_msg(0);
ros::Time time_last_odometry_msg(0);
double time_since_last_vehicle_command = NAN;
double vehicle_command_timeout = 0.4;  // 10 lost messages if frequency is 25 Hz
double time_since_last_heart_beat = NAN;
double heart_beat_timeout = 1;  // 10 lost messages if frequency is 10 Hz
double time_since_last_odometry = NAN;
double odometry_timeout = 1;  // 100 lost messages if frequency is 100 Hz

double max_speed = 0;
bool external_ok_to_drive = false;
bool is_emergency_stop_service_called = false;
double navigation_speed = 0;
double wheel_average_speed = 0;
double difference_speed = 0;
double max_difference_speed = 1;
std::string speed_too_high_emergency_stop_string;

class EmergencyStopHandler
{
public:

  EmergencyStopHandler()
  { emergencyStop = false; }

  void setEmergencyStop()
  { emergencyStop = true; };

  void clearEmergencyStop()
  {
    emergencyStop = false;
  };

  bool isEmergencyStop()
  { return emergencyStop; };

private:
  bool emergencyStop;
} emergencyStop;


void dynamic_reconfig_callback(olav_controller::VehicleSetupConfig& config, uint32_t level)
{
  if (olav != nullptr)
  {
    olav->olavCommand.minCommandedSpeedBeforeBraking = config.min_commanded_speed;
    olav->olavCommand.maxCommandedSpeed = config.max_commanded_speed;
    olav->olavCommand.maxCommandedAcceleration = config.max_commanded_acceleration;
    olav->olavCommand.maxCommandedThrottleEffort = config.max_throttle_effort;
    olav->olavCommand.setMaxCommandedSteeringAngleRad(config.max_commanded_steering_angle);
    olav->olavCommand.setMaxCommandedSteeringVelocityRadPrSec(config.max_commanded_steering_velocity);
    olav->olavCommand.maxCommandedSteeringEffort = config.max_steering_effort;
    olav->olavCommand.maxRpmInGearChange = config.max_rpm_in_gear_change;

    olav->olavMeasurements.setSteeringMeasurement_mVCenter(config.steering_measurment_mV_center);
    olav->olavMeasurements.setSteeringMeasurement_mVPrRad(config.steering_measurment_mV_per_rad);

    max_speed = config.max_permitted_speed;
    max_difference_speed = config.max_permitted_speed_difference;
  }
}


void vehicleCommandMsgCallback(const olav_msgs::vehicle_command msg)
{
  // If any of values is NAN, pretend as not received message (ready to drive flag is false and brakes on)
  if (std::isnan(msg.ackermann_drive.acceleration) ||
      std::isinf(msg.ackermann_drive.acceleration) ||
      std::isnan(msg.ackermann_drive.jerk) ||
      std::isinf(msg.ackermann_drive.jerk) ||
      std::isnan(msg.ackermann_drive.speed) ||
      std::isinf(msg.ackermann_drive.speed) ||
      std::isnan(msg.ackermann_drive.steering_angle) ||
      std::isinf(msg.ackermann_drive.steering_angle) ||
      std::isnan(msg.ackermann_drive.steering_angle_velocity) ||
      std::isinf(msg.ackermann_drive.steering_angle_velocity))
  {
    ROS_WARN_STREAM("Olav controller received NAN or INFINITY as a command, Breaks on and wait");
  }
  else
  {
    currentVehicleCommand = msg;
    time_last_vehicle_command_msg = ros::Time::now();
  }
}


void heartBeatMsgCallback(const std_msgs::Header msg)
{
  time_last_heartbeat_msg = ros::Time::now();
}


void odometryMsgCallback(const nav_msgs::Odometry msg)
{
  if (msg.child_frame_id == "B")
  {
    time_last_odometry_msg = ros::Time::now();

    navigation_speed = msg.twist.twist.linear.x;

    tf::Quaternion q(msg.pose.pose.orientation.x,
                     msg.pose.pose.orientation.y,
                     msg.pose.pose.orientation.z,
                     msg.pose.pose.orientation.w);

    tf::Matrix3x3 m1(q);
    double roll, pitch, heading;
    m1.getRPY(roll, pitch, heading);

    olav->olavMeasurements.roll = roll;
    olav->olavMeasurements.pitch = pitch;
    olav->olavMeasurements.heading = heading;
    olav->timeLastOdometryMsg = ros::Time::now();

    ROS_DEBUG_THROTTLE(THROTTLE_RATE, "Odometry Message Received");
  }
  else
  {
    ROS_WARN("Olav controller is subscribing to wrong odometry message");
  }
}


bool emergencyStopService(std_srvs::Empty::Request& req, std_srvs::Empty::Response& resp)
{
  is_emergency_stop_service_called = true;
  emergencyStop.setEmergencyStop();
//  ROS_ERROR("Emergency stop, service called");

  olav_diagnostics->updateEmergencyStopReasonIsEmergencyStopServiceCalled(true);
  return true;
}


bool clearEmergencyStopService(std_srvs::Empty::Request& req, std_srvs::Empty::Response& resp)
{
  is_emergency_stop_service_called = false;
  emergencyStop.clearEmergencyStop();
//  ROS_WARN("Cleared emergency stop, service called");

  olav_diagnostics->updateEmergencyStopReasonIsEmergencyStopServiceCalled(false);
  return true;
}


bool startService(std_srvs::Empty::Request& req, std_srvs::Empty::Response& resp)
{
  external_ok_to_drive = true;
  ROS_INFO("Olav Controller start service called");

  return true;
}


bool stopService(std_srvs::Empty::Request& req, std_srvs::Empty::Response& resp)
{
  external_ok_to_drive = false;
  ROS_INFO("Olav Controller stop service called");

  return true;
}


bool isReceivingHearbeats()
{
  time_since_last_heart_beat = (ros::Time::now() - time_last_heartbeat_msg).toSec();

  if (time_since_last_heart_beat < heart_beat_timeout)
    return true;
  else
  {
    ROS_DEBUG_THROTTLE(THROTTLE_RATE, "Receiving Hearbeats failed (dt = %.2f sec > %.2f sec)", time_since_last_heart_beat, heart_beat_timeout);
    return false;
  }
}


bool isReceivingVehicleCommands()
{
  time_since_last_vehicle_command = (ros::Time::now() - time_last_vehicle_command_msg).toSec();

  if (time_since_last_vehicle_command < vehicle_command_timeout)
    return true;
  else
  {
    ROS_DEBUG_THROTTLE(THROTTLE_RATE, "Receiving Vehicle Commands failed (dt = %.2f sec > %.2f sec)", time_since_last_vehicle_command, vehicle_command_timeout);
    return false;
  }
}


bool isReceivingOdometry()
{
  time_since_last_odometry = (ros::Time::now() - time_last_odometry_msg).toSec();

  if (time_since_last_odometry < odometry_timeout)
    return true;
  else
  {
    ROS_WARN_THROTTLE(30, "Receiving Odometry failed (dt = %.2f sec > %.2f sec)", time_since_last_odometry, odometry_timeout);
    return false;
  }
}


bool isSpeedTooHigh()
{
  wheel_average_speed = olav->getAckermannDriveAverage().speed;

  if (!isReceivingOdometry())
  {
    navigation_speed = 0;
  }

  difference_speed = fabs(wheel_average_speed - fabs(navigation_speed));

  bool speed_too_high = false;
  speed_too_high_emergency_stop_string.clear();

  if (wheel_average_speed >= max_speed)
  {
    speed_too_high = true;
    speed_too_high_emergency_stop_string += "\n- Wheel speed was too high";
  }

  if (fabs(navigation_speed) >= max_speed)
  {
    speed_too_high = true;
    speed_too_high_emergency_stop_string += "\n- Navigation system speed was too high";
  }

  if (difference_speed >= max_difference_speed)
  {
    speed_too_high = true;
    speed_too_high_emergency_stop_string += "\n- Difference between wheel speed and navigation system speed was too high";
  }

  if (speed_too_high)
  {
    return true;
  }
  else
  {
    return false;
  }
}


VehicleStates translateToVehicleStates(uint8_t state)
{
  switch (state)
  {
    case olav_msgs::vehicle_state::VEHICLE_OFF:
      return VEHICLE_OFF;
    case olav_msgs::vehicle_state::VEHICLE_ON_PARK:
      return VEHICLE_ON_PARK;
    case olav_msgs::vehicle_state::VEHICLE_ON_REVERSE:
      return VEHICLE_ON_REVERSE;
    case olav_msgs::vehicle_state::VEHICLE_ON_LOW:
      return VEHICLE_ON_LOW;
    case olav_msgs::vehicle_state::VEHICLE_ON_HIGH:
      return VEHICLE_ON_HIGH;
    default:
      return VEHICLE_OFF;
  }
}


uint8_t translateFromVehicleStates(VehicleStates state)
{
  switch (state)
  {
    case NO_STATE :
      return olav_msgs::vehicle_state::NO_STATE;
    case EMERGENCY_STOP :
      return olav_msgs::vehicle_state::EMERGENCY_STOP;
    case MANUAL_MODE :
      return olav_msgs::vehicle_state::MANUAL_MODE;
    case VEHICLE_OFF :
      return olav_msgs::vehicle_state::VEHICLE_OFF;
    case VEHICLE_ON_PARK :
      return olav_msgs::vehicle_state::VEHICLE_ON_PARK;
    case VEHICLE_ON_REVERSE :
      return olav_msgs::vehicle_state::VEHICLE_ON_REVERSE;
    case VEHICLE_ON_LOW :
      return olav_msgs::vehicle_state::VEHICLE_ON_LOW;
    case VEHICLE_ON_HIGH :
      return olav_msgs::vehicle_state::VEHICLE_ON_HIGH;
    default:
      return olav_msgs::vehicle_state::NO_STATE;
  }
}


bool isDrivableGear()
{
  return olav->getCurrentVehicleState() == Vehicle::ENGINE_ON_REVERSE ||
         olav->getCurrentVehicleState() == Vehicle::ENGINE_ON_LOW ||
         olav->getCurrentVehicleState() == Vehicle::ENGINE_ON_HIGH;
}

bool isExternalOkToDrive()
{
  olav_diagnostics->updateIsOkToDriveReasonExternalOkToDrive(external_ok_to_drive);
  return external_ok_to_drive;
}


bool isOKtoDrive()
{
  bool okToDrive = !emergencyStop.isEmergencyStop() &&
                   olav->isAutonomousMode() &&
                   isDrivableGear() &&
                   isReceivingVehicleCommands() &&
                   isReceivingOdometry() &&
                   isReceivingHearbeats();

  olav_diagnostics->updateIsOkToDrive(okToDrive);
  olav_diagnostics->updateIsOkToDriveReasonIsAutonomousMode(olav->isAutonomousMode());
  olav_diagnostics->updateIsOkToDriveReasonIsDrivableGear(isDrivableGear());
  olav_diagnostics->updateIsOkToDriveReasonIsReceivingVehicleComands(isReceivingVehicleCommands(), time_since_last_vehicle_command);
  olav_diagnostics->updateIsOkToDriveReasonIsReceivingHeartbeats(isReceivingHearbeats(), time_since_last_heart_beat);
  olav_diagnostics->updateIsOkToDriveReasonIsReceivingOdometryMessages(isReceivingOdometry(), time_since_last_odometry);

  return okToDrive;
}


void printIsOkToDriveMessage()
{
  if (!isOKtoDrive())
  {
    ROS_WARN_STREAM_THROTTLE(THROTTLE_RATE,
                             "" << "Not ok to drive because:"
                                << (emergencyStop.isEmergencyStop() ? "\n- Olav Controller is in emergency stop" : "")
                                << (!external_ok_to_drive ? "\n- Start service call has not been called" : "")
                                << (!isDrivableGear() ? "\n- Olav is not in drivable gear" : "")
                                << (olav->isManualMode() ? "\n- Is in manual mode" : "")
                                << (!isReceivingVehicleCommands() ? "\n- Is not receiving vehicle commands" : "")
                                << (!isReceivingHearbeats() ? "\n- Olav Controller is not receiving heartbeats" : "")
                                << (!isReceivingOdometry()
                                    ? "\n- Olav Controller is not receiving odometry navigation messages" : ""));
  }
  else if (isOKtoDrive() && !isExternalOkToDrive())
  {
    ROS_WARN_STREAM_THROTTLE(THROTTLE_RATE, "Waiting for start service call");
  }
}


bool isEmergencyStopPresent()
{
  bool emergency_stop_present = olav->isEmergencyStop() ||
                                !olav->arduinoIsOK() ||
                                !olav->modbusIsOK() ||
                                isSpeedTooHigh() ||
                                is_emergency_stop_service_called;

  olav_diagnostics->updateEmergencyStop(emergencyStop.isEmergencyStop());
  olav_diagnostics->updateEmergencyStopReasonIsEmergencyStopButtonPressed(olav->isEmergencyStop());
  olav_diagnostics->updateEmergencyStopReasonIsArduinoOk(olav->arduinoIsOK());
  olav_diagnostics->updateEmergencyStopReasonIsMoodusOk(olav->modbusIsOK());
  olav_diagnostics->updateEmergencyStopReasonIsSpeedOk(!isSpeedTooHigh());
  olav_diagnostics->updateEmergencyStopReasonIsEmergencyStopServiceCalled(is_emergency_stop_service_called);

  return emergency_stop_present;
}


void printEmergencyStopMessage()
{
  if (isEmergencyStopPresent())
  {
    ROS_ERROR_STREAM_THROTTLE(THROTTLE_RATE,
                              "" << "Emergency stop because:"
                                 << (olav->isEmergencyStop() ? "\n- Emergency stop button is pressed" : "")
                                 << (!olav->arduinoIsOK() ? "\n- Olav Controller has no connection with Arduino" : "")
                                 << (!olav->modbusIsOK() ? "\n- Olav Controller has no connection with PLC" : "")
                                 << (isSpeedTooHigh() ? speed_too_high_emergency_stop_string : "")
                                 << (is_emergency_stop_service_called ? "\n- Emergency stop service was called" : ""));
  }
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "olav_controller");
  ros::NodeHandle nh;

  ROS_INFO("olav_controller starting up");

  // Make this thread high priority
  struct sched_param param;
  param.sched_priority = 99;

  if (sched_setscheduler(0, SCHED_FIFO, &param) != 0)
  {
    ROS_INFO("sched_setscheduler: %s\n", strerror(errno));
  }
  else
  {
    ROS_INFO("sched_setscheduler: Success\n");
  }

  // Create Olav
  olav = new Vehicle(LOOP_RATE);

  // Parameters
  bool use_belts = false;
  std::string topic_vehicle_command;
  std::string topic_vehicle_status;
  std::string topic_heart_beat;
  std::string topic_odometry;
  std::string topic_vehicle_debug;
  std::string topic_emergency_stop;
  std::string topic_clear_emergency_stop;
  std::string topic_vehicle_measurements;
  std::string topic_hal_set_manual_mode;
  std::string topic_hal_set_autonomous_mode;
  std::string topic_start;
  std::string topic_stop;
  double publish_vehicle_debug_rate;
  double publish_vehicle_status_rate;
  double publish_vehicle_measurements_rate;

  ros::NodeHandle privateNH("~");
  privateNH.getParam("vehicle_setup/use_belts", use_belts);

  privateNH.getParam("subscribe/vehicle_command", topic_vehicle_command);
  privateNH.getParam("subscribe/vehicle_command_timeout", vehicle_command_timeout);
  privateNH.getParam("subscribe/heart_beat", topic_heart_beat);
  privateNH.getParam("subscribe/heart_beat_timeout", heart_beat_timeout);
  privateNH.getParam("subscribe/odometry", topic_odometry);
  privateNH.getParam("subscribe/odometry_timeout", odometry_timeout);

  privateNH.getParam("publish/vehicle_debug", topic_vehicle_debug);
  privateNH.getParam("publish/vehicle_debug_rate", publish_vehicle_debug_rate);
  privateNH.getParam("publish/vehicle_status", topic_vehicle_status);
  privateNH.getParam("publish/vehicle_status_rate", publish_vehicle_status_rate);
  privateNH.getParam("publish/vehicle_measurements", topic_vehicle_measurements);
  privateNH.getParam("publish/vehicle_measurements_rate", publish_vehicle_measurements_rate);

  privateNH.getParam("service/olav/emergency_stop", topic_emergency_stop);
  privateNH.getParam("service/olav/clear_emergency_stop", topic_clear_emergency_stop);
  privateNH.getParam("service/olav/start", topic_start);
  privateNH.getParam("service/olav/stop", topic_stop);
  privateNH.getParam("service/hal/set_manual_mode", topic_hal_set_manual_mode);
  privateNH.getParam("service/hal/set_autonomous_mode", topic_hal_set_autonomous_mode);

  ros::Publisher pub_vehicle_status = nh.advertise<olav_msgs::vehicle_status>(topic_vehicle_status, 10);
  ros::Publisher pub_vehicle_debug = nh.advertise<olav_msgs::vehicle_debug>(topic_vehicle_debug, 10);
  ros::Publisher pub_vehicle_measurements = nh.advertise<ackermann_msgs::AckermannDriveStamped>(topic_vehicle_measurements, 10);
  ros::Subscriber sub_olav_command = nh.subscribe(topic_vehicle_command, 1, &vehicleCommandMsgCallback);
  ros::Subscriber sub_heart_beat = nh.subscribe(topic_heart_beat, 1, &heartBeatMsgCallback);
  ros::Subscriber sub_odometry = nh.subscribe(topic_odometry, 1, &odometryMsgCallback);

  ROS_DEBUG_STREAM("Subscribing to " << topic_vehicle_command);
  ROS_DEBUG_STREAM("Subscribing to " << topic_heart_beat);
  ROS_DEBUG_STREAM("Subscribing to " << topic_odometry);

  ros::ServiceServer srv_emergency_stop = privateNH.advertiseService(topic_emergency_stop, &emergencyStopService);
  ros::ServiceServer srv_clear_emergency_stop = privateNH.advertiseService(topic_clear_emergency_stop, &clearEmergencyStopService);
  ros::ServiceServer srv_start = privateNH.advertiseService(topic_start, &startService);
  ros::ServiceServer srv_stop = privateNH.advertiseService(topic_stop, &stopService);

  std_srvs::Empty emptyService;

  // Init dynamic reconfigure
  ros::NodeHandle control_nh(privateNH, "vehicle_setup");
  dynamic_reconfigure::Server<olav_controller::VehicleSetupConfig> server(control_nh);
  dynamic_reconfigure::Server<olav_controller::VehicleSetupConfig>::CallbackType f;
  f = boost::bind(&dynamic_reconfig_callback, _1, _2);
  server.setCallback(f);

  // Diagnostics
  diagnostic_updater::Updater diagUpdater;
  diagUpdater.setHardwareID("none");
  olav_diagnostics = new OlavDiagnostics(diagUpdater);

  diagnostic_updater::HeaderlessTopicDiagnostic diag_vehicle_status("Published vehicle_status", diagUpdater,
                                                                    diagnostic_updater::FrequencyStatusParam(
                                                                        &publish_vehicle_status_rate,
                                                                        &publish_vehicle_status_rate));

  diagnostic_updater::HeaderlessTopicDiagnostic diag_vehicle_measurements("Published vehicle_measurements", diagUpdater,
                                                                          diagnostic_updater::FrequencyStatusParam(
                                                                              &publish_vehicle_measurements_rate,
                                                                              &publish_vehicle_measurements_rate));

  olav_diagnostics->updateEmergencyStopReasonIsEmergencyStopServiceCalled(false);

  ros::Rate loop_rate(LOOP_RATE);
  ROS_INFO_STREAM("Going to work, loop rate: " << LOOP_RATE);
  ROS_INFO_STREAM("Olav Controller is in: " << std::string(use_belts ? "BELT MODE" : "WHEEL MODE"));

  VehicleStates vehicleState = NO_STATE;
  VehicleStates prevVehicleState = NO_STATE;
  VehicleStates nextVehicleState = NO_STATE;

  ros::Time time_start_work;
  ros::Time timestamp_vehicle_debug;
  ros::Time timestamp_vehicle_status;
  ros::Time timestamp_vehicle_measurements;
  ros::Time time_start_work_last(0);
  ros::Time timestamp_vehicle_debug_last(0);
  ros::Time timestamp_vehicle_status_last(0);
  ros::Time timestamp_vehicle_measurements_last(0);
  double last_loop_time;

  while (ros::ok())
  {
    // Timers
    time_start_work = ros::Time::now();
    last_loop_time = (time_start_work - time_start_work_last).toSec();

    time_start_work_last = time_start_work;

    // Check for emergency stop, except when olav is in NO_STATE or in MANUAL_MODE
    if (isEmergencyStopPresent() && vehicleState != NO_STATE && vehicleState != MANUAL_MODE)
    {
      emergencyStop.setEmergencyStop();
      printEmergencyStopMessage();
    }
    else if (emergencyStop.isEmergencyStop() && !isEmergencyStopPresent())
    {
      ROS_WARN_STREAM_THROTTLE(THROTTLE_RATE, "Emergency stop condition is not present, waiting on reset");
    }


    if (emergencyStop.isEmergencyStop())
    {
      vehicleState = EMERGENCY_STOP;
    }


    // Check if Olav is in manual mode or autonomous mode
    if (olav->isManualMode())
    {
      vehicleState = MANUAL_MODE;
    }


    // If Olav is not OK to drive, reset start service call
    // Another way to to this: vehicleState != VEHICLE_ON_PARK || vehicleState != VEHICLE_ON_REVERSE || vehicleState != VEHICLE_ON_LOW || vehicleState != VEHICLE_ON_HIGH
    if (!isOKtoDrive())
    {
      external_ok_to_drive = false;
    }


    // State machine
    switch (vehicleState)
    {
      case NO_STATE:

        if (isReceivingVehicleCommands())
        {
          if (currentVehicleCommand.vehicle_state.vehicle_state == olav_msgs::vehicle_state::VEHICLE_OFF)
          {
            prevVehicleState = NO_STATE;
            vehicleState = VEHICLE_OFF;
            nextVehicleState = vehicleState;
          }
          else
          {
            ROS_INFO_THROTTLE(THROTTLE_RATE, "Olav is in NO_STATE, waiting on VEHICLE_OFF command");
          }
        }
        else
        {
          ROS_INFO_THROTTLE(THROTTLE_RATE, "Olav is in NO_STATE, waiting on vehicle_command messages");
        }

        if (olav->arduinoIsOK() && olav->modbusIsOK())
        {
          olav->breaksOnAndReset();
        }

        break;
      case EMERGENCY_STOP:

        if (emergencyStop.isEmergencyStop())
        {
          olav->emergencyStop();

          // First entry
          if (prevVehicleState != vehicleState)
          {
            ROS_ERROR("Olav is in EMERGENCY_STOP");

            nextVehicleState = vehicleState;
            prevVehicleState = vehicleState;
          }
        }
        else
        {
          vehicleState = NO_STATE;
        }

        break;
      case MANUAL_MODE:

        if (olav->isManualMode())
        {
          // First entry
          if (prevVehicleState != vehicleState)
          {
            ROS_WARN("Olav is in manual mode");

            olav->resetControllers();
            nextVehicleState = VEHICLE_ON_PARK;
            prevVehicleState = vehicleState;
            if (!ros::service::call(topic_hal_set_manual_mode, emptyService))
            {
              ROS_ERROR_STREAM("Couldn't call " << topic_hal_set_manual_mode << " service in HAL");
            };
          }
        }
        else
        {
          olav->breaksOnAndReset();
          if (!ros::service::call(topic_hal_set_autonomous_mode, emptyService))
          {
            ROS_ERROR_STREAM("Couldn't call " << topic_hal_set_autonomous_mode << " service in HAL");
          };

          ROS_WARN_STREAM("Olav is in autonomous mode, going to " << nextVehicleState);
          vehicleState = nextVehicleState;
        }

        break;
      case VEHICLE_OFF:

        // First entry
        if (prevVehicleState != vehicleState)
        {
          ROS_INFO("Olav is in VEHICLE_OFF");
          prevVehicleState = vehicleState;
        }

        nextVehicleState = translateToVehicleStates(currentVehicleCommand.vehicle_state.vehicle_state);
        if (vehicleState != nextVehicleState)
        {
          vehicleState = nextVehicleState;
        }

        olav->breaksOnAndReset();

        break;
      case VEHICLE_ON_PARK:

        // First entry
        if (prevVehicleState != vehicleState)
        {
          ROS_INFO("Olav is in VEHICLE_ON_PARK");
          prevVehicleState = vehicleState;
        }

        nextVehicleState = translateToVehicleStates(currentVehicleCommand.vehicle_state.vehicle_state);
        if (vehicleState != nextVehicleState)
        {
          vehicleState = nextVehicleState;
        }

        olav->breaksOnAndReset();

        break;
      case VEHICLE_ON_REVERSE:

        // First entry
        if (prevVehicleState != vehicleState)
        {
          ROS_INFO("Olav is in VEHICLE_ON_REVERSE");
          prevVehicleState = vehicleState;
        }

        nextVehicleState = translateToVehicleStates(currentVehicleCommand.vehicle_state.vehicle_state);
        if (vehicleState != nextVehicleState)
        {
          olav->breaksOnAndReset();
          vehicleState = nextVehicleState;
        }
        else if (isOKtoDrive() && isExternalOkToDrive())
        {
          // ROS_INFO_THROTTLE(THROTTLE_RATE, "Olav is in VEHICLE_ON_REVERSE, and is driving");
          olav->setTargets(currentVehicleCommand.ackermann_drive.speed,
                           currentVehicleCommand.ackermann_drive.acceleration,
                           currentVehicleCommand.ackermann_drive.steering_angle,
                           currentVehicleCommand.ackermann_drive.steering_angle_velocity);
        }
        else
        {
          olav->breaksOnAndReset();
          printIsOkToDriveMessage();
        }

        break;
      case VEHICLE_ON_LOW:

        // First entry
        if (prevVehicleState != vehicleState)
        {
          ROS_INFO("Olav is in VEHICLE_ON_LOW");
          prevVehicleState = vehicleState;
        }

        nextVehicleState = translateToVehicleStates(currentVehicleCommand.vehicle_state.vehicle_state);
        if (vehicleState != nextVehicleState)
        {
          olav->breaksOnAndReset();
          vehicleState = nextVehicleState;
        }
        else if (isOKtoDrive() && isExternalOkToDrive())
        {
          // ROS_INFO_THROTTLE(THROTTLE_RATE, "Olav is in VEHICLE_ON_LOW, and is driving");
          olav->setTargets(currentVehicleCommand.ackermann_drive.speed,
                           currentVehicleCommand.ackermann_drive.acceleration,
                           currentVehicleCommand.ackermann_drive.steering_angle,
                           currentVehicleCommand.ackermann_drive.steering_angle_velocity);
        }
        else
        {
          olav->breaksOnAndReset();
          printIsOkToDriveMessage();
        }

        break;
      case VEHICLE_ON_HIGH:

        // First entry
        if (prevVehicleState != vehicleState)
        {
          ROS_INFO("Olav is in VEHICLE_ON_HIGH");
          prevVehicleState = vehicleState;
        }

        nextVehicleState = translateToVehicleStates(currentVehicleCommand.vehicle_state.vehicle_state);
        if (vehicleState != nextVehicleState)
        {
          olav->breaksOnAndReset();
          vehicleState = nextVehicleState;
        }
        else if (isOKtoDrive() && isExternalOkToDrive())
        {
          // ROS_INFO_THROTTLE(THROTTLE_RATE, "Olav is in VEHICLE_ON_HIGH, and is driving");
          olav->setTargets(currentVehicleCommand.ackermann_drive.speed,
                           currentVehicleCommand.ackermann_drive.acceleration,
                           currentVehicleCommand.ackermann_drive.steering_angle,
                           currentVehicleCommand.ackermann_drive.steering_angle_velocity);
        }
        else
        {
          olav->breaksOnAndReset();
          printIsOkToDriveMessage();
        }

        break;
    }


    olav->updateInternalState(vehicleState);
    olav->updatePLC();

    // Update ackermann_drive_msg data
    olav->updateMeasurements();

    // Publish vehicle_debug with desired frequency
    olav_msgs::vehicle_debug debug_msg;
    debug_msg.header.stamp = ros::Time::now();

    debug_msg.emergency_stop = emergencyStop.isEmergencyStop();
    debug_msg.autonomous_mode = olav->isAutonomousMode();
    debug_msg.ok_to_drive = isOKtoDrive();
    debug_msg.start_service_called = isExternalOkToDrive();

    debug_msg.vehicle_state_previous.vehicle_state = prevVehicleState;
    debug_msg.vehicle_state_current.vehicle_state = vehicleState;
    debug_msg.vehicle_state_next.vehicle_state = nextVehicleState;
    debug_msg.vehicle_internal_state_previous.vehicle_internal_state = olav->getPrevVehicleState();
    debug_msg.vehicle_internal_state_current.vehicle_internal_state = olav->getCurrentVehicleState();
    debug_msg.vehicle_internal_state_next.vehicle_internal_state = olav->getNextVehicleState();

    debug_msg.controller_dt = olav->olavCommand.dt.toSec();
    debug_msg.max_commanded_target_speed = olav->olavCommand.maxCommandedSpeed;
    debug_msg.commanded_target_speed = currentVehicleCommand.ackermann_drive.speed;
    debug_msg.target_speed = olav->olavCommand.targetSpeed;

    debug_msg.max_commanded_target_acceleration = olav->olavCommand.maxCommandedAcceleration;
    debug_msg.commanded_target_acceleration = currentVehicleCommand.ackermann_drive.acceleration;
    debug_msg.target_acceleration = olav->olavCommand.targetAcceleration;

    debug_msg.min_commanded_target_speed_before_braking = olav->olavCommand.minCommandedSpeedBeforeBraking;

    debug_msg.setpoint_speed = olav->olavCommand.setpointSpeed;
    debug_msg.use_feed_forward = olav->olavCommand.useFeedForward;
    debug_msg.inclination = olav->olavCommand.inclination;
    debug_msg.feed_forward_effort = olav->olavCommand.feedforwardEffort;
    debug_msg.speed = olav->olavMeasurements.getSpeedMs();
    debug_msg.speed_error = olav->olavCommand.speedError;
    debug_msg.pid_throttle_effort = olav->olavCommand.pidThrottleEffort;
    debug_msg.max_commanded_throttle_effort = olav->olavCommand.maxCommandedThrottleEffort;
    debug_msg.throttle_effort = olav->olavCommand.throttleEffort;
    debug_msg.break_effort = olav->olavCommand.breakEffort;

    debug_msg.max_commanded_target_steering_angle_deg = olav->olavCommand.getMaxCommandedSteeringAngleDeg();
    debug_msg.commanded_target_steering_angle_deg =
        currentVehicleCommand.ackermann_drive.steering_angle * 180 / M_PI;
    debug_msg.target_steering_angle_deg = olav->olavCommand.getTargetSteeringAngleDeg();

    debug_msg.max_commanded_target_steering_velocity_deg_pr_sec = olav->olavCommand.getMaxCommandedSteeringVelocityDegPrSec();
    debug_msg.commanded_target_steering_velocity_deg_pr_sec =
        currentVehicleCommand.ackermann_drive.steering_angle_velocity * 180 / M_PI;
    debug_msg.target_steering_velocity_deg_pr_sec = olav->olavCommand.getTargetSteeringVelocityDegPrSec();

    debug_msg.setpoint_steering_angle_deg = olav->olavCommand.getSetpointSteeringAngleDeg();
    debug_msg.steering_angle_deg = olav->olavMeasurements.getSteeringAngleDeg();
    debug_msg.steering_error = olav->olavCommand.steeringAngleErrorDeg;
    debug_msg.pid_steering_effort = olav->olavCommand.pidSteeringEffort;
    debug_msg.steering_effort = olav->olavCommand.steeringEffort;
    debug_msg.max_commanded_steering_effort = olav->olavCommand.maxCommandedSteeringEffort;

    debug_msg.ignition_on = olav->olavMeasurements.isIgnitionOn;
    debug_msg.current_rpm = olav->olavMeasurements.rpm;
    debug_msg.current_speed_axel_rad_sec = olav->olavMeasurements.getAxelRadPrSec();
    debug_msg.current_gear = olav->olavMeasurements.gear;
    debug_msg.current_gear_actuator_position = olav->olavMeasurements.gearActuatorPosition;
    debug_msg.current_is_gear_in_positon = olav->olavMeasurements.isGearInPosition;

    debug_msg.last_loop_time = last_loop_time;
//        debug_msg.last_working_time = last_working_time;
//        debug_msg.last_sleep_time = last_sleep_time;
    debug_msg.last_loop_rate = 1 / last_loop_time;

    debug_msg.ackermann_drive_command = currentVehicleCommand.ackermann_drive;
    debug_msg.ackermann_drive_measured_full_rate = olav->getAckermannDriveFullRate();
    debug_msg.ackermann_drive_measured_average = olav->getAckermannDriveAverage();

    debug_msg.navigation_speed = navigation_speed;
    debug_msg.wheel_speed = wheel_average_speed;
    debug_msg.speed_difference = difference_speed;
    debug_msg.max_speed = max_speed;
    debug_msg.max_absolute_speed_difference = max_difference_speed;

    pub_vehicle_debug.publish(debug_msg);

    // Publish vehicle_status with desired frequency
    ros::Time timestamp_vehicle = ros::Time::now();
    if ((timestamp_vehicle - timestamp_vehicle_status_last).toSec() >= 1 / publish_vehicle_status_rate)
    {
      timestamp_vehicle_status_last = timestamp_vehicle;

      olav_msgs::vehicle_status vehicle_status;
      vehicle_status.header.stamp = timestamp_vehicle;
      vehicle_status.vehicle_state.vehicle_state = translateFromVehicleStates(vehicleState);
      vehicle_status.ok_to_drive = isOKtoDrive();
      vehicle_status.start_service_called = isExternalOkToDrive();
      pub_vehicle_status.publish(vehicle_status);
      diag_vehicle_status.tick();
    }

    // Publish vehicle_measurements with desired frequency
    ros::Time timestamp_vehicle_measurements = ros::Time::now();
    if ((timestamp_vehicle_measurements - timestamp_vehicle_measurements_last).toSec() >=
        1 / publish_vehicle_measurements_rate)
    {
      timestamp_vehicle_measurements_last = timestamp_vehicle_measurements;

      ackermann_msgs::AckermannDriveStamped vehicle_measurements;
      vehicle_measurements.header.stamp = timestamp_vehicle_measurements;
      vehicle_measurements.drive = olav->getAckermannDriveAverage();
      pub_vehicle_measurements.publish(vehicle_measurements);
      diag_vehicle_measurements.tick();
    }

    olav_diagnostics->updateVehicleState(vehicleState, nextVehicleState, prevVehicleState);
    olav_diagnostics->updateInternalVehicleStates(olav->getCurrentVehicleState(), olav->getNextVehicleState(),
                                                  olav->getPrevVehicleState());


    olav_diagnostics->updateVehicleMeasurements(olav->olavMeasurements.isIgnitionOn,
                                                olav->olavMeasurements.rpm,
                                                olav->olavMeasurements.gear,
                                                olav->olavMeasurements.getSpeedMs());

    diagUpdater.update();

    ros::spinOnce();
    loop_rate.sleep();
  }

  delete olav;
  return 0;
}
