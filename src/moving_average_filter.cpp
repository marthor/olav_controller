#include "olav_controller/moving_average_filter.h"


MovingAverageFilter::MovingAverageFilter() : buffer_(1)
{}


MovingAverageFilter::MovingAverageFilter(const size_t& filter_size) : buffer_(filter_size)
{}

void MovingAverageFilter::clearFilter()
{
  buffer_.clear();
}


void MovingAverageFilter::setFilterSize(const size_t& filter_size)
{
  buffer_.clear();
  buffer_.set_capacity(filter_size);
}


double MovingAverageFilter::getMovingAverage(const double& value)
{
  buffer_.push_back(value);
  //print();
  return calculateMovingAverage();
}


double MovingAverageFilter::calculateMovingAverage()
{
  double moving_average = 0;
  for (auto elem:buffer_)
  {
    moving_average += elem;
  }
  return moving_average / buffer_.size();
}


void MovingAverageFilter::print()
{
  bool first = true;
  std::cout << "mean = (";
  for (auto elem:buffer_)
  {
    if (first)std::cout << std::to_string(elem);
    else std::cout << " + " << std::to_string(elem);
    first = false;
  }

  std::cout << ") / " << std::to_string(buffer_.size()) << " = " << std::to_string(calculateMovingAverage())
            << std::endl;
}