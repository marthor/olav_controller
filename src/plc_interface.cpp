#include "olav_controller/plc_interface.h"


PlcInterface::PlcInterface(std::string address, int port,
                           VehicleMeasurements* olavMeasurements)
    :
    connected(false),
    ticker(true)
{
  // Allocate modbus context
  this->olavMeasurements = olavMeasurements;
  this->address = address;
  this->port = port;
  this->modbusContext = modbus_new_tcp(this->address.c_str(), this->port);
  if (modbusContext == NULL)
  {
    throw PlcInterfaceException("Failed to allocate Modbus context.");
  }
}


PlcInterface::~PlcInterface()
{
  // Close connection if open
  if (this->isOpen())
  {
    this->close();
  }
  // Free modbus context memory
  modbus_free(this->modbusContext);
}


// Open connection to PLC
void PlcInterface::open()
{
  // Do nothing if already connected
  if (this->isOpen())
  {
    return;
  }

  // Try to connect to PLC
  if (modbus_connect(this->modbusContext) == -1)
  {
    this->connected = false;
    throw PlcInterfaceException("Failed to connect to Modbus server.");
  } else
  {
    this->connected = true;
  }
}


// Close connection to PLC
void PlcInterface::close()
{
  modbus_close(this->modbusContext);
  modbus_flush(this->modbusContext);
  this->connected = false;
}


// Update vehicle data and PLC
void PlcInterface::writeData(const double& steeringEffort, const double& breakEffort, const double& throttleEffort,
                             const bool& ignitionOn, const bool& emergencyStop, const  bool& starterEngineOn, const Gears& selectGear)
{
  int registerWriteOffset = 0x200;
  int writtenRegisters = 0;

  // Write data registers to PLC
  int16_t writeBuffer[8];
  writeBuffer[0] = -static_cast<int16_t>(steeringEffort);
  writeBuffer[1] = static_cast<int16_t>(breakEffort);
  writeBuffer[2] = static_cast<int16_t>(throttleEffort);
  writeBuffer[3] = static_cast<int16_t>(ignitionOn ? 1 : 0);
  writeBuffer[4] = static_cast<int16_t>(ignitionOn ? 0 : 1);
  writeBuffer[5] = static_cast<int16_t>(emergencyStop ? 1 : 0);
  writeBuffer[6] = static_cast<int16_t>(starterEngineOn ? 1 : 0);
  writeBuffer[7] = selectGear;

//  writeBuffer[0] = dataToPlc.steeringEffort;
//  writeBuffer[1] = dataToPlc.breakEffort;
//  writeBuffer[2] = dataToPlc.throttleEffort;
//  writeBuffer[3] = dataToPlc.ignitionOn ? 1 : 0;
//  writeBuffer[4] = dataToPlc.ignitionOn ? 0 : 1;
//  writeBuffer[5] = dataToPlc.emergencyStopOn ? 1 : 0;
//  writeBuffer[6] = dataToPlc.starterEngineOn ? 1 : 0;
//  writeBuffer[7] = dataToPlc.selectGear;

  writtenRegisters += modbus_write_registers(this->modbusContext, registerWriteOffset + 0x100, 8,
                                             (uint16_t*) writeBuffer);

  // Invert ticker and write to PLC
  this->ticker = !this->ticker;
  writtenRegisters += modbus_write_register(this->modbusContext, registerWriteOffset + 0x10c, this->ticker ? 1 : 0);

  // Check if there where read or write errors
  if (writtenRegisters != 8 + 1)
  {
    // Close connection and throw an error
    this->close();
    throw PlcInterfaceException("Communication failed!");
  }
}


void PlcInterface::readData()
{
  int registerReadOffset = 0x100;
  int readRegisters = 0;
  int arrayLength = 7;

  // Read registers from PLC
  int16_t readBuffer[arrayLength];
  readRegisters += modbus_read_registers(this->modbusContext, registerReadOffset, arrayLength, (uint16_t*) readBuffer);

  // Check if there where read or write errors
  if (readRegisters != arrayLength)
  {
    // Close connection and throw an error
    this->close();
    throw PlcInterfaceException("Communication failed!");
  }

  // Store data in vehicleData object
/*  this->vehicleData->currentSteeringAngleRaw = readBuffer[0];
  this->vehicleData->currentGear = readBuffer[1];
  this->vehicleData->currentBreakPos = readBuffer[3];
  this->vehicleData->currentIgnitionOn = readBuffer[5];
  this->vehicleData->currentAutoSwitch = readBuffer[6];
  */

  olavMeasurements->setSteeringAngleRaw(static_cast<int>(readBuffer[0]));
  olavMeasurements->gear = static_cast<Gears>(readBuffer[1]);
  olavMeasurements->gearActuatorPosition = static_cast<int>(readBuffer[2]);
  olavMeasurements->brakePos= static_cast<int>(readBuffer[3]);
  olavMeasurements->isIgnitionOn = readBuffer[5]!= 0;

  std::bitset<16> bit_array(readBuffer[6]);
  olavMeasurements->isAutonomousModeOn  = bit_array.test(0);
  olavMeasurements->isGearInPosition  = bit_array.test(1);
}


void PlcInterface::enableSteeringPid(int p, int i, int d)
{
  int registerWriteOffset = 0x200;
  int writtenRegisters;
  int16_t writeBuffer[4];
  writeBuffer[0] = p;
  writeBuffer[1] = i;
  writeBuffer[2] = d;
  writeBuffer[3] = 1;
  writtenRegisters = modbus_write_registers(this->modbusContext, registerWriteOffset + 0x109, 4,
                                            (uint16_t*) writeBuffer);

  // Check if there where read or write errors
  if ((writtenRegisters != 4))
  {
    // Close connection and throw an error
    this->close();
    throw PlcInterfaceException("Communication failed!");
  }
}


void PlcInterface::disableSteeringPid()
{
  int registerWriteOffset = 0x200;
  int writtenRegisters;
  int16_t writeBuffer = 0;
  writtenRegisters = modbus_write_register(this->modbusContext, registerWriteOffset + 0x10C, writeBuffer);

  // Check if there where read or write errors
  if ((writtenRegisters != 1))
  {
    // Close connection and throw an error
    this->close();
    throw PlcInterfaceException("Communication failed!");
  }
}
