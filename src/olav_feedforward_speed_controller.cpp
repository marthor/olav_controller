#include "olav_controller/olav_feedforward_speed_controller.h"
#include <boost/bind.hpp>
#include <boost/make_shared.hpp>
#include <boost/algorithm/clamp.hpp>


namespace olav_controller
{
FeedforwardSpeedController::FeedforwardSpeedController(ros::NodeHandle& nodeHandle)
    : node(nodeHandle)
{
  this->initDynamicReconfig();
  this->updateConfig();
}


double FeedforwardSpeedController::computeCommand(double targetSpeedMS, int gear, double inclination)
{
  // Do nothing if feed forward is disabled in config
  if (!this->config.enable_feedforward)
  {
    return 0.0;
  }

  // Calculate target speed in radians
  double targetSpeed = targetSpeedMS / this->config.r_eff;

  // Find transmission speed ratio
  double R_t = 0.0;
  switch (gear)
  {
    case 2:
      inclination = -inclination;
      R_t = this->config.speed_ratio_reverse;
      break;
    case 4:
      R_t = this->config.speed_ratio_low;
      break;
    case 5:
      R_t = this->config.speed_ratio_high;
      break;
  }

  inclination += this->config.inclination_correction;

  // Calculate forces acting on vehicle at steady state
  double forceDrag = this->config.air_resistance * targetSpeed * targetSpeed +
                     this->config.rolling_resistance * cos(inclination) * this->config.mass;
  double forceGravity = sin(inclination) * this->config.mass * 9.81;
  double targetForce = forceDrag + forceGravity;
  double torqueSense = 0.0;
  if (targetForce > 0)
  {
    torqueSense = targetForce * this->config.r_dyn / R_t;
  }

  double feedforwardThrottle = 0.0;
  double feedforwardBrake = 0.0;
  double targetEngineSpeed;
  double R_cvt;

  // Calculate steady state engine speed at zero throttle
  targetEngineSpeed = (this->config.cvt_speed_ratio_max -
                       (this->config.cvt_speed_ratio_max - this->config.cvt_speed_ratio_min) *
                       (-this->config.engine_speed_belt_engagement / this->config.cvt_upshift_sensitivity -
                        torqueSense / this->config.cvt_downshift_sensitivity)) /
                      (this->config.r_eff / (R_t * targetSpeed) +
                       (this->config.cvt_speed_ratio_max - this->config.cvt_speed_ratio_min) /
                       this->config.cvt_upshift_sensitivity);
  //
  // Calculate and clamp cvt speed ratio
  R_cvt = boost::algorithm::clamp(this->config.cvt_speed_ratio_max -
                                  (this->config.cvt_speed_ratio_max - this->config.cvt_speed_ratio_min) *
                                  ((targetEngineSpeed - this->config.engine_speed_belt_engagement) /
                                   this->config.cvt_upshift_sensitivity -
                                   torqueSense / this->config.cvt_downshift_sensitivity),
                                  this->config.cvt_speed_ratio_min, this->config.cvt_speed_ratio_max);

  // Recalculate engine speed with clamped cvt speed ratio
  targetEngineSpeed = R_cvt * R_t * targetSpeed / this->config.r_eff;

  // If throttle is enough to regulate speed
  if (((-targetEngineSpeed * this->config.engine_friction * R_cvt * R_t / this->config.r_dyn) < targetForce) &&
      (targetEngineSpeed > this->config.engine_speed_belt_engagement))
  {
    // Calculate feed forward throttle
    feedforwardThrottle = 100.0 * ((targetForce * this->config.r_dyn) / (R_cvt * R_t) +
                                   this->config.engine_friction * targetEngineSpeed) /
                          (P1 + (P2 + this->config.engine_friction) * targetEngineSpeed +
                           P3 * targetEngineSpeed * targetEngineSpeed);
    feedforwardThrottle = boost::algorithm::clamp(feedforwardThrottle, 0.0, 100.0);
  } else
  {
    // Calculate feed forward brake
    if (targetEngineSpeed > this->config.engine_speed_belt_engagement)
    {
      targetForce += this->config.engine_friction * targetEngineSpeed;
    }
    feedforwardBrake = boost::algorithm::clamp(
        -targetForce / (this->config.max_brake_torque / this->config.r_dyn) * (100.0 - this->config.min_brake_percent) +
        this->config.min_brake_percent, 0, 100.0);
  }

  ROS_DEBUG_STREAM_THROTTLE_NAMED(1, "feedforward_speed_controller", "Speed:" << targetSpeed
                                                                              << " Engine speed:" << targetEngineSpeed
                                                                              << " Effort:"
                                                                              << feedforwardThrottle - feedforwardBrake
                                                                              << " inclination:" << inclination
                                                                              << " F_d:" << forceDrag
                                                                              << " F_gx:" << forceGravity);

  return feedforwardThrottle - feedforwardBrake;
}


void FeedforwardSpeedController::initDynamicReconfig()
{
  ROS_DEBUG_STREAM_NAMED("feedforward_speed_controller",
                         "Initialized dynamic reconfigure in namespace" << this->node.getNamespace());

  reconfigServer = boost::make_shared<dynamic_reconfigure::Server<FeedforwardSpeedControllerConfig> >(this->node);
  dynamic_reconfigure::Server<FeedforwardSpeedControllerConfig>::CallbackType f;
  f = boost::bind(&FeedforwardSpeedController::dynamicReconfigCallback, this, _1);
  reconfigServer->setCallback(f);
}


void FeedforwardSpeedController::dynamicReconfigCallback(FeedforwardSpeedControllerConfig config)
{
  this->config = config;
  this->updateConfig();
}


void FeedforwardSpeedController::updateConfig()
{
  this->P1 = 1.0e3 * this->config.engine_max_power / this->config.engine_speed_max_power;
  this->P2 = 1.0e3 * this->config.engine_max_power /
             (this->config.engine_speed_max_power * this->config.engine_speed_max_power);
  this->P3 = 1.0e3 * this->config.engine_max_power /
             (this->config.engine_speed_max_power * this->config.engine_speed_max_power *
              this->config.engine_speed_max_power);
}

}